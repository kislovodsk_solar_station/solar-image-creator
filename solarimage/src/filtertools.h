#ifndef FILTERTOOLS_H
#define FILTERTOOLS_H

#include <QWidget>

namespace Ui {
class FilterTools;
}

class FilterTools : public QWidget
{
    Q_OBJECT

public:
    explicit FilterTools(QWidget *parent = 0);
    ~FilterTools();
protected:
    virtual void mouseMoveEvent(QMouseEvent *);
    virtual void mousePressEvent(QMouseEvent *);
private slots:
    void on_spinBox_valueChanged(int arg1);

    void on_spinBox_2_valueChanged(int arg1);

    void on_spinBox_3_valueChanged(int arg1);

    void on_spinBox_4_valueChanged(int arg1);

    void on_spinBox_5_valueChanged(int arg1);

    void on_spinBox_6_valueChanged(int arg1);

    void on_checkBox_toggled(bool checked);

    void on_checkBox_2_toggled(bool checked);

    void on_spinBox_7_valueChanged(int arg1);

public slots:
    void setLimits ( int x1 = 0, int x2 = 0, int y1 = 0, int y2 = 0);

signals:
    void sigInterpolPointsCountChanged( int ) ;
    void sigTranspChanged(int);
    void sigLimx1Changed(int);
    void sigLimx2Changed(int);
    void sigLimy1Changed(int);
    void sigLimy2Changed(int);
    void sigContrastChanged(int);
    void sigHorizontFiltChanged(bool);
    void sigVerticalFiltChanged(bool);
private:
    Ui::FilterTools *ui;
    int x1;
    int y1;
};

#endif // FILTERTOOLS_H
