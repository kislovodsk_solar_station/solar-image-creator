// Класс работы с изображением Солнца (построение и обработка)

#ifndef SOLARIMAGECREATOR_H
#define SOLARIMAGECREATOR_H

#include "imagetiff.h"
#include "fitsfile.h"

#include <QObject>
#include <QPixmap>
#include <QImage>
#include <QThread>

#include <stdint.h>
//#include "tiffio.h"
#include "spline.h"
#include <memory>
#include <iostream>

typedef struct
{
    double x;
    double y;
}points_t;

class SolarImageCreator : public QObject
{
    Q_OBJECT
    cubic_spline spline;
    int width;
    int height;
    uint32_t bytesCount;
    uint32_t wordsCount;
    uint8_t* bytesBuff; // Буфер для 8-бит изображения для записи на диск.
    uint16_t* wordsBuff; // Буфер для 16-бит изображения для записи на диск.
    int k;
    int aprxPointsCount;
    QWidget* wdgt;  // Не исп.
    QPoint* midPoints; // буфер для хранения координаты средней линии.
    QPoint * points1;
    bool isCentralLineSet; // Установлена ли средняя линия.
    bool isReady; // Состояние работы задачи. Готовность.
    QImage filteredImg;

    QThread* thread;
    QPainter* painter;

    // Координаты центра линии поглощения
    points_t* mPts;
    QPoint* midPoints1;
    QPoint* midPoints2;

    QPoint* hPoints;
    QPoint* hPoints2;
    QPoint* hPoints3;
    QPoint* hPoints4;

    QPoint* vPoints;
    QPoint* vPoints2;
    QPoint* vPoints3;
    QPoint* vPoints4;

    int totalPoints;
    int totalPoints1;
    int totalPoints2;

    int hTotalPoints;
    int hTotalPoints2;
    int hTotalPoints3;
    int hTotalPoints4;

    int vTotalPoints;
    int vTotalPoints2;
    int vTotalPoints3;
    int vTotalPoints4;

    int pCount;
    int transparency;
    int limx1;
    int limx2;
    int limy1;
    int limy2;
    double contrast;
    bool isHorizontFilter;
    bool isVerticalFilter;

public:
    SolarImageCreator();
    double Lagranj (double X, double * x, double* y, int size );
    ~SolarImageCreator();
    //uint GetTotalFrames();
    QString filename;
public slots:
    void SetCentralLine(QString m_filename, int offset);
    void CreateImage();
    void CreateDoplerImage();
    void CreateDoplerImage2();
    QString FindLongestSpectorFile(QString path);
    void FilterImage();
    void TranspChanged(int);
    void PointsCountChanged(int);
    void onLimx1Changed(int);
    void onLimx2Changed(int);
    void onLimy1Changed(int);
    void onLimy2Changed(int);
    void onContrastChanged(int);
    void onSaveImage(QString m_filename, QString type);
    void onHorizontFiltChanged(bool);
    void onVerticalFiltChanged(bool);
    void CalcSpectroFocus();
signals:
    void sigFinished();
    void sigStarted();
    void sigImageReady(QPixmap);
    void sigShowSpectroFocus( QPixmap,
                              QPoint*, int,
                              QPoint*, int);
    void sigCentralLineReady( QPixmap,
                              QPoint*, int,
                              QPoint*, int );
    void sigFilteredImageReady( QPixmap,
                                QPoint*, int,
                                QPoint*, int,
                                QPoint*, int,
                                QPoint*, int,
                                QPoint*, int,
                                QPoint*, int,
                                QPoint*, int,
                                QPoint*, int,
                                QPoint*, int,
                                QPoint*, int );
    void sigProcessState(int, QString );
    void sigTiffImageSaved(QString &);
    void sigInitLimits( int, int, int, int);
    void sigSetFilteToolsVisible(bool);
    void sigWarningMessage(QString strmsg);
};

#endif // SOLARIMAGECREATOR_H
