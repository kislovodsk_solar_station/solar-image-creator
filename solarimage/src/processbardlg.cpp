#include "processbardlg.h"
#include "ui_processbardlg.h"
#include <QString>

ProcessBarDlg::ProcessBarDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProcessBarDlg)
{
    ui->setupUi(this);
    ui->progressBar->setValue(0);
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(0);
}

ProcessBarDlg::~ProcessBarDlg()
{
    delete ui;
}

void ProcessBarDlg::setValue(int val, QString text)
{
    QString tmp = "";
    ui->progressBar->setValue(val);
    if ( ui->progressBar->maximum() == val )
    {
        ui->progressBar->setValue(0);
        //this->hide();
    }
    int k = text.indexOf('\n');
    int n = text.length() - k;
    tmp.append( text.left(k + ui->label->width() / 5));
    if ( text.length() - tmp.length() > 15)
    {
        tmp.append("...");
        tmp.append( text.right(15));
    }
    else
    {
        tmp.clear();
        tmp.append(text);
    }
    ui->label->setText(tmp);
}

void ProcessBarDlg::setMaximum(int val)
{
    ui->progressBar->setMaximum(val);
}

void ProcessBarDlg::showDlg()
{
    this->move(this->parentWidget()->x() + this->parentWidget()->width()/2  - this->width()/2,
               this->parentWidget()->y() + this->parentWidget()->height()/2 - this->height()/2 );
    this->show();

}
