#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include "solarimagecreatordlg.h"
#include "solarimagecreator.h"
#include "processbardlg.h"
#include <QMessageBox>
#include <QDebug>
#include <QString>
#include <QDateTime>
#include <QDir>
#include <QMdiArea>
#include "solarguidewdgt.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    SolarGuideWdgt * solarGuideWdgt;
    SolarImageCreator * solarImageCreator;
    ProcessBarDlg * processBarDlg;
    SolarImageCreatorDlg * solarImageCreatorDlg;
    QThread * threadImageCreator;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QString imagePath;
private slots:
    void ShowImageView();
    void ShowCentralLine();
    void FilterImage();
    void CreateDoplerImage();
public slots:
    void ShowStatusMessage( char * msg);
    void ShowWarningMessage( QString str );
    void CreateNewSolarImageDlg(uint16_t num);
    void DeleteSolarImageDlg(uint16_t num);
signals:
    void sigCreateDoplerImage();
    void sigCreateImage();
    void sigSetCenralLine(QString, int);
    void sigFilterImage();
};

#endif // MAINWINDOW_H
