#ifndef SAVETOOLS_H
#define SAVETOOLS_H

#include <QWidget>
#include <QFileDialog>

namespace Ui {
class SaveTools;
}

class SaveTools : public QWidget
{
    Q_OBJECT

public:
    explicit SaveTools(QWidget *parent = 0);
    ~SaveTools();

private:
    Ui::SaveTools *ui;
signals:
    void sigSaveImage(QString, QString);
private slots:
    void on_pushButton_clicked();
};

#endif // SAVETOOLS_H
