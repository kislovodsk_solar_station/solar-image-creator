//Cubic spline interpolation program
//when we have two columns of data x and y in input file:
//
//x0 y0
//x1 y1
//...
//xn yn
//
//and we want to find such function f(x)  
//where f(xi) = yi
//and f(x) is cubic function on every [x_k-1, x_k] segment
//and f(x), f'(x), f''(x) are continual
//the result is four columns of cubic polinom coefficients

#include "spline.h"

double *x, *y, *h, *l, *delta, *lambda, *c, *d, *b;
int N;
char filename[256];
FILE* InFile = NULL;

void AllocMatrix();
void PrintResult();

//------------------------------------------------------------------
//
//
//
void AllocMatrix()
{
    //allocate memory for matrixes
    x = new double[N+1];
    y = new double[N+1];
    h = new double[N+1];
    l = new double[N+1];
    delta = new double[N+1];
    lambda = new double[N+1];
    c = new double[N+1];
    d = new double[N+1];
    b = new double[N+1];
}

//------------------------------------------------------------------
//
//
//
void FreeMatrix()
{
    delete [] x;
    delete [] y;
    delete [] h;
    delete [] l;
    delete [] delta;
    delete [] lambda;
    delete [] c;
    delete [] d;
    delete [] b;
}

//------------------------------------------------------------------
//
//
//
void PrintResult()
{
    printf("\nA[k]\tB[k]\tC[k]\tD[k]\n");
    for(int k = 1; k <= N; k++)
    {
        printf("%f\t%f\t%f\t%f\n", y[k], b[k], c[k], d[k]);
    }
}

//------------------------------------------------------------------
//
//
//
double GetSpline(double s)
{
    //float start = x[0];
    //float end = x[N];
    //float step = (end - start)/20;
    //FILE* OutFile = fopen("test.txt", "wt");
    //for( float s = start; s <= end; s += step)
    //{
        //find k, where s in [x_k-1; x_k]
        int k = 0;
        for( k = 1; k <= N; k++)
        {
            if(s >= x[k-1] && s <= x[k])
			{
                break;
            }
        }
        double F = y[k] + b[k]*(s-x[k]) + c[k]*pow(s-x[k], 2) + d[k]*pow(s-x[k], 3);
        //fprintf(OutFile, "%f\t%f\n", s,  F);
    //}
    //fclose(OutFile);
    return F;
}



//------------------------------------------------------------------
//
//
//
void CreateSpline( double * x1, double * y1, int n )
{
    int k = 0;
    N = n;
    AllocMatrix();
    int i = 0;
    //read matrixes a and b from input file
    for(i = 0; i < n; i++)
    {
        x[i] = x1[i];
        y[i] = y1[i];
    }

    // не установлены значения... Выходим
    if ( N == 0)
        return;
    for(k = 1; k <= N; k++)
    {
        h[k] = x[k] - x[k-1];
        if(h[k] == 0)
        {
            printf("\nError, x[%d]=x[%d]\n", k, k-1);
            return;
        }
        l[k] = (y[k] - y[k-1])/h[k];
    }
    delta[1] = - h[2]/(2*(h[1]+h[2]));
    lambda[1] = 1.5*(l[2] - l[1])/(h[1]+h[2]);
    for(k = 3; k <= N; k++)
    {
        delta[k-1] = - h[k]/(2*h[k-1] + 2*h[k] + h[k-1]*delta[k-2]);
        lambda[k-1] = (3*l[k] - 3*l[k-1] - h[k-1]*lambda[k-2]) /
                (2*h[k-1] + 2*h[k] + h[k-1]*delta[k-2]);
    }
    c[0] = 0;
    c[N] = 0;
    for(k=N; k>=2; k--)
    {
        c[k-1] = delta[k-1]*c[k] + lambda[k-1];
    }
    for(k=1; k<=N; k++)
    {
        d[k] = (c[k] - c[k-1])/(3*h[k]);
        b[k] = l[k] + (2*c[k]*h[k] + h[k]*c[k-1])/3;
    }
//    PrintResult();
//    testresult();
//    freematrix();
}

cubic_spline::cubic_spline() : splines(NULL)
{

}

cubic_spline::~cubic_spline()
{
    free_mem();
}

void cubic_spline::build_spline(const double *x, const double *y, std::size_t n)
{
    free_mem();

    this->n = n;

    // Инициализация массива сплайнов
    splines = new spline_tuple[n];
    for (std::size_t i = 0; i < n; ++i)
    {
        splines[i].x = x[i];
        splines[i].a = y[i];
    }
    splines[0].c = 0.;

    // Решение СЛАУ относительно коэффициентов сплайнов c[i] методом прогонки для трехдиагональных матриц
    // Вычисление прогоночных коэффициентов - прямой ход метода прогонки
    double *alpha = new double[n - 1];
    double *beta = new double[n - 1];
    double A, B, C, F, h_i, h_i1, z;
    alpha[0] = beta[0] = 0.;
    for (std::size_t i = 1; i < n - 1; ++i)
    {
        h_i = x[i] - x[i - 1], h_i1 = x[i + 1] - x[i];
        A = h_i;
        C = 2. * (h_i + h_i1);
        B = h_i1;
        F = 6. * ((y[i + 1] - y[i]) / h_i1 - (y[i] - y[i - 1]) / h_i);
        z = (A * alpha[i - 1] + C);
        alpha[i] = -B / z;
        beta[i] = (F - A * beta[i - 1]) / z;
    }

    splines[n - 1].c = (F - A * beta[n - 2]) / (C + A * alpha[n - 2]);

    // Нахождение решения - обратный ход метода прогонки
    for (std::size_t i = n - 2; i > 0; --i)
        splines[i].c = alpha[i] * splines[i + 1].c + beta[i];

    // Освобождение памяти, занимаемой прогоночными коэффициентами
    delete[] beta;
    delete[] alpha;

    // По известным коэффициентам c[i] находим значения b[i] и d[i]
    for (std::size_t i = n - 1; i > 0; --i)
    {
        double h_i = x[i] - x[i - 1];
        splines[i].d = (splines[i].c - splines[i - 1].c) / h_i;
        splines[i].b = h_i * (2. * splines[i].c + splines[i - 1].c) / 6. + (y[i] - y[i - 1]) / h_i;
    }
}

double cubic_spline::f(double x) const
{
    if (!splines)
        return std::numeric_limits<double>::quiet_NaN(); // Если сплайны ещё не построены - возвращаем NaN

    spline_tuple *s;
    if (x <= splines[0].x) // Если x меньше точки сетки x[0] - пользуемся первым эл-том массива
        s = splines + 1;
    else if (x >= splines[n - 1].x) // Если x больше точки сетки x[n - 1] - пользуемся последним эл-том массива
        s = splines + n - 1;
    else // Иначе x лежит между граничными точками сетки - производим бинарный поиск нужного эл-та массива
    {
        std::size_t i = 0, j = n - 1;
        while (i + 1 < j)
        {
            std::size_t k = i + (j - i) / 2;
            if (x <= splines[k].x)
                j = k;
            else
                i = k;
        }
        s = splines + j;
    }

    double dx = (x - s->x);
    return s->a + (s->b + (s->c / 2. + s->d * dx / 6.) * dx) * dx; // Вычисляем значение сплайна в заданной точке.
}

void cubic_spline::free_mem()
{
    delete[] splines;
    splines = NULL;
}
