#ifndef FITSFILE_H
#define FITSFILE_H

#include <QObject>

#include <string.h>
#include <stdlib.h>
//#include <stdio.h>
#include <fitsio.h>
#include <stdint.h>
//#include <inttypes.h>

class FitsFile : public QObject
{
    Q_OBJECT
public:
    explicit FitsFile(QObject *parent = 0);
    int CreateFitsFile(const char * fitsname, uint32_t size, unsigned short * buf);
    fitsfile *fptr; /* pointer to the FITS file; defined in fitsio.h */
    int status ;
    long fpixel;
    long naxis;
    long nelements;
    long exposure;
    long naxes[2];
    unsigned short * array;
signals:

public slots:
};

#endif // FITSFILE_H
