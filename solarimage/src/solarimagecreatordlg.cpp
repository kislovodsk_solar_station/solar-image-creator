#include "SolarImageCreatorDlg.h"
#include "ui_SolarImageCreatorDlg.h"

#include <QDebug>
#include <QLibrary>
#include <QLine>
#include <windows.h>
#include <iostream>
#include <QSize>

#define SCAN_DIR "C:/Projects/201509220444/obs01/"
#define SCAN_IMG "C:/Projects/201509220444/obs01/sun.bmp"
#define UP_FILTER_LIMIT 55
#define POINT_RAD 4

using namespace  std;
uint16_t SolarImageCreatorDlg::number = 0;
//-----------------------------------------------------------------------------
// Конструктор класса
//
SolarImageCreatorDlg::SolarImageCreatorDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SolarImageCreatorDlg)
{
    ui->setupUi(this);
    setAutoFillBackground(true);
    this->setWindowFlags(Qt::Window);
    tiffimg = new ImageTiff;
    mylbl = new MyLabel();
    fTools = new FilterTools(ui->scrollArea);
    sTools = new SaveTools(ui->scrollArea);
    sTools->setFixedWidth(fTools->width());
    isSetPosition = false;
    connect(fTools, SIGNAL(sigInterpolPointsCountChanged(int)),
            this, SIGNAL(sigPointsCountChanged(int)));
    connect(fTools, SIGNAL(sigTranspChanged(int)),
            this, SIGNAL(sigTranspChanged(int)));
    connect(sTools, SIGNAL(sigSaveImage(QString, QString)),
            this, SIGNAL(sigSaveImage(QString, QString)));
    connect(fTools, SIGNAL(sigLimx1Changed(int)),
            this, SIGNAL(sigLimx1Changed(int)));
    connect(fTools, SIGNAL(sigLimx2Changed(int)),
            this, SIGNAL(sigLimx2Changed(int)));
    connect(fTools, SIGNAL(sigLimy1Changed(int)),
            this, SIGNAL(sigLimy1Changed(int)));
    connect(fTools, SIGNAL(sigLimy2Changed(int)),
            this, SIGNAL(sigLimy2Changed(int)));
    connect(fTools, SIGNAL(sigContrastChanged(int)),
            this, SIGNAL(sigContrastChanged(int)));
    connect(fTools, SIGNAL(sigHorizontFiltChanged(bool)),
            this, SIGNAL(sigHorizontFiltChanged(bool)));
    connect(fTools, SIGNAL(sigVerticalFiltChanged(bool)),
            this, SIGNAL(sigVerticalFiltChanged(bool)));
    pix = new QPixmap;
    number++;
    qDebug() << "SolarImageCreatorDlg, number = " << number;
    sTools->hide();
    fTools->hide();
    ui->widget->hide();
}
//-----------------------------------------------------------------------------
//
//
SolarImageCreatorDlg::~SolarImageCreatorDlg()
{
    number--;
    qDebug() << "~SolarImageCreatorDlg, number = " << number;
    tiffimg->PurgeBuffer();
    delete ui;
}

//-----------------------------------------------------------------------------
//
//
//
void SolarImageCreatorDlg::SetCentralLine( QPixmap pixmap,
                                           QPoint* points, int totPoints,
                                           QPoint* points1, int totPoints1)
{
    mylbl->PurgeFuncs();
    mylbl->func1 = points;
    mylbl->totalPointsfunc1 = totPoints;
    mylbl->points1 = points1;
    mylbl->totalPoints1 = totPoints1;
    mylbl->setFixedSize( pixmap.width(), pixmap.height() );

    ui->scrollArea->setWidget(mylbl);
    mylbl->setPixmap(pixmap);
    mylbl->repaint();
    mylbl->update();
    //fTools->move(ui->scrollArea->width() - fTools->width() - 20, 50);
    fTools->hide();
    sTools->hide();
    ui->widget->hide();
    if ( isSetPosition == false)
    {
        setGeometry( x(), y(), mylbl->width() + 20, mylbl->height() + 20);
        //move( parentWidget()->x() - (width()/2 - parentWidget()->width()/2),
        //      parentWidget()->y());
        isSetPosition = true;
    }
    parentWidget()->resize(this->width() + 20, this->height() + 60);
    show();
    parentWidget()->show();
    parentWidget()->showMaximized();

}

//-----------------------------------------------------------------------------
//
//
//
void SolarImageCreatorDlg::ShowSpectroFocus( QPixmap pixmap,
                                             QPoint* points, int totPoints,
                                             QPoint* points1, int totPoints1)
{
    isSetPosition = false;
    mylbl->PurgeFuncs();
    mylbl->func1 = points;
    mylbl->totalPointsfunc1 = totPoints;
    mylbl->func2 = points1;
    mylbl->totalPointsfunc2 = totPoints1;
    mylbl->setFixedSize( pixmap.width(), pixmap.height() );
    ui->scrollArea->setWidget(mylbl);
    mylbl->setPixmap(pixmap);
    mylbl->repaint();
    mylbl->update();
    //fTools->move(ui->scrollArea->width() - fTools->width() - 20, 50);
    fTools->hide();
    sTools->hide();
    ui->widget->hide();
    show();
    parentWidget()->show();
    this->setGeometry(this->x(), this->y(), pixmap.width() + 20, pixmap.height() + 20);
    //this->move(this->parentWidget()->x() - (this->width()/2 - this->parentWidget()->width()/2), this->y());

}

//-----------------------------------------------------------------------------
//
//  Вывод спектрогелиограммы после сборки
//
void SolarImageCreatorDlg::ShowImage(QPixmap pixmap)
{
    isSetPosition = false;
    mylbl->PurgeFuncs();
    mylbl->setFixedSize( pixmap.width(), pixmap.height() );
    ui->scrollArea->setWidget ( mylbl );
    mylbl->setPixmap ( pixmap );

    show();
    parentWidget()->show();
    mylbl->repaint();
    mylbl->update();
    //this->setGeometry(this->parentWidget()->x(),
    //                  this->parentWidget()->y(),
    //                  this->parentWidget()->width(),
    //                  this->parentWidget()->height());
    //this->move(this->parentWidget()->x() - this->parentWidget()->y(), this->this->y());
    //fTools->move(ui->scrollArea->width() - fTools->width() - 20, 50);
    //    fTools->setVisible(false);
    //    sTools->show( );
    //    ui->widget->setVisible(false);
    //    sTools->move(ui->scrollArea->width() - sTools->width() - 20, 50);
    mylbl->repaint();
    mylbl->update();
    fTools->move(ui->scrollArea->width() - fTools->width() - 20, 50);
    sTools->move(ui->scrollArea->width() - sTools->width() - 20, 50);// fTools->y() + fTools->height() + 5);
    fTools->hide();
    sTools->show();
    ui->widget->hide();
    *pix = pixmap;

}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreatorDlg::initLimits( int x1, int x2, int y1, int y2)
{
    fTools->setLimits(x1, x2, y1, y2);
}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreatorDlg::ShowFilteredImage ( QPixmap pixmap,
                                               QPoint* hP1, int hTotP1,
                                               QPoint* hP2, int hTotP2,
                                               QPoint* hP3, int hTotP3,
                                               QPoint* hP4, int hTotP4,
                                               QPoint* vP1, int vTotP1,
                                               QPoint* vP2, int vTotP2,
                                               QPoint* vP3, int vTotP3,
                                               QPoint* vP4, int vTotP4,
                                               QPoint* mP1, int mP1Tot,
                                               QPoint* mP2, int mP2Tot)
{
    isSetPosition = false;
    mylbl->totalPoints1 = mP1Tot;
    mylbl->totalPoints2 = mP2Tot;
    mylbl->totalPointsfunc1 = hTotP1;
    mylbl->totalPointsfunc2 = hTotP2;
    mylbl->totalPointsfunc3 = hTotP3;
    mylbl->totalPointsfunc4 = hTotP4;
    mylbl->totalPointsfunc5 = vTotP1;
    mylbl->totalPointsfunc6 = vTotP2;
    mylbl->totalPointsfunc7 = vTotP3;
    mylbl->totalPointsfunc8 = vTotP4;
    mylbl->points1 = mP1;
    mylbl->points2 = mP2;
    mylbl->func1 = hP1;
    mylbl->func2 = hP2;
    mylbl->func3 = hP3;
    mylbl->func4 = hP4;
    mylbl->func5 = vP1;
    mylbl->func6 = vP2;
    mylbl->func7 = vP3;
    mylbl->func8 = vP4;
    mylbl->setFixedSize(pixmap.width(), pixmap.height());
    //ui->solarWidget->setFixedSize(pix.width(), pix.height());
    ui->scrollArea->setWidget(mylbl);
    mylbl->setPixmap(pixmap);
    show();
    parentWidget()->show();
    //    this->setGeometry(this->parentWidget()->x(),
    //                      this->parentWidget()->y(),
    //                      this->parentWidget()->width(),
    //                      this->parentWidget()->height());
    //this->move(this->parentWidget()->x() - this->parentWidget()->y(), this->this->y());
    mylbl->repaint();
    mylbl->update();
    fTools->move(ui->scrollArea->width() - fTools->width() - 20, 50);
    fTools->show();
    sTools->move(ui->scrollArea->width() - sTools->width() - 20, fTools->y() + fTools->height() + 5);
    sTools->show();
    ui->widget->show();
}



//-----------------------------------------------------------------------------
//
//
void SolarImageCreatorDlg::SetFilterToolsVisible(bool b)
{
    fTools->setVisible(b);
}


//-----------------------------------------------------------------------------
//
//
void SolarImageCreatorDlg::paintEvent(QPaintEvent* e)
{
    if ( fTools->isVisible())
    {
        fTools->move(ui->scrollArea->width() - fTools->width() - 20, 50);
        sTools->move(ui->scrollArea->width() - sTools->width() - 20, fTools->y() + fTools->height() + 5);
    }
    else
        sTools->move(ui->scrollArea->width() - sTools->width() - 20, 50);
}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreatorDlg::resizeEvent(QResizeEvent * ev)
{
    if ( fTools->isVisible())
    {
        fTools->move(ui->scrollArea->width() - fTools->width() - 20, 50);
        sTools->move(ui->scrollArea->width() - sTools->width() - 20, fTools->y() + fTools->height() + 5);
    }
    else
        sTools->move(ui->scrollArea->width() - sTools->width() - 20, 50);

}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreatorDlg::closeEvent(QCloseEvent * ev)
{

}




//-----------------------------------------------------------------------------
//
//
void MyLabel::wheelEvent(QWheelEvent * event)
{
    if (QApplication::keyboardModifiers() == Qt::ControlModifier)
    {
        // Если нажата клавиша CTRL этот код выполнится
        //QPixmap pix = *this->pixmap();
        //pix = pix.scaled(pix.width() - 10, pix.height(), Qt::KeepAspectRatio, Qt::SmoothTransformation) ;
        //setPixmap(pix);
        QSize size( this->width(), this->height());
        QPoint p = event->angleDelta();
        if ( p.y() < 0 )
        {
            size.scale(this->width() - 10, this->height(),Qt::KeepAspectRatio);
        }
        else if ( p.y() > 0  )
        {
            size.scale(this->width() + 10, this->height(),Qt::KeepAspectRatioByExpanding);
        }
        //this->setFixedWidth(this->width() - 10);
        this->setFixedSize(size.width(), size.height());
    }
}

//-----------------------------------------------------------------------------
//
//
void MyLabel::paintEvent(QPaintEvent* e)
{
    QLabel::paintEvent(e);
    QPainter p(this);
    p.setRenderHint(QPainter::HighQualityAntialiasing, true);
    p.setPen(QPen(Qt::green, 1, Qt::SolidLine));
    for ( int i = 0; i < totalPoints1 ; i++)
    {
        p.drawLine(points1[i].x() - POINT_RAD, points1[i].y() + POINT_RAD,
                   points1[i].x() + POINT_RAD, points1[i].y() - POINT_RAD );
        p.drawLine(points1[i].x() - POINT_RAD, points1[i].y() - POINT_RAD,
                   points1[i].x() + POINT_RAD, points1[i].y() + POINT_RAD );
        //p.drawPoints(points1, totalPoints1);
    }
    for ( int i = 0; i < totalPoints2 ; i++)
    {
        p.drawLine(points2[i].x() - POINT_RAD, points2[i].y() + POINT_RAD,
                   points2[i].x() + POINT_RAD, points2[i].y() - POINT_RAD );
        p.drawLine(points2[i].x() - POINT_RAD, points2[i].y() - POINT_RAD,
                   points2[i].x() + POINT_RAD, points2[i].y() + POINT_RAD );
        //p.drawPoints(points2, totalPoints2);
    }

    p.setPen(QColor(255,0,255));
    //p.drawPoints(points, *totalPoints);
    for ( int i = 1; i < totalPointsfunc1 ; i++)
    {
        p.drawLine(func1[i - 1].x(), func1[i - 1].y(), func1[i].x(), func1[i].y());
    }
    for ( int i = 1; i < totalPointsfunc5 ; i++)
    {
        p.drawLine(func5[i - 1].x(), func5[i - 1].y(), func5[i].x(), func5[i].y());
    }
    p.setPen(Qt::green);
    //p.drawPoints(points, *totalPoints);
    for ( int i = 1; i < totalPointsfunc2 ; i++)
    {
        p.drawLine(func2[i - 1].x(), func2[i - 1].y(), func2[i].x(), func2[i].y());
    }
    for ( int i = 1; i < totalPointsfunc6 ; i++)
    {
        p.drawLine(func6[i - 1].x(), func6[i - 1].y(), func6[i].x(), func6[i].y());
    }
    p.setPen(Qt::yellow);
    //p.drawPoints(points, *totalPoints);
    for ( int i = 1; i < totalPointsfunc4 ; i++)
    {
        p.drawLine(func4[i - 1].x(), func4[i - 1].y(), func4[i].x(), func4[i].y());
    }
    for ( int i = 1; i < totalPointsfunc8 ; i++)
    {
        p.drawLine(func8[i - 1].x(), func8[i - 1].y(), func8[i].x(), func8[i].y());
    }
    p.setPen(QColor(255,128,0));
    //p.drawPoints(points, *totalPoints);
    for ( int i = 1; i < totalPointsfunc3 ; i++)
    {
        p.drawLine(func3[i - 1].x(), func3[i - 1].y(), func3[i].x(), func3[i].y());
    }
    for ( int i = 1; i < totalPointsfunc7 ; i++)
    {
        p.drawLine(func7[i - 1].x(), func7[i - 1].y(), func7[i].x(), func7[i].y());
    }


}




