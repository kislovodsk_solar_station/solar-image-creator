//------------------------------------------------------------------------
// Класс для вывода изображения Солнца и отметки центра солнечного диска
// Разработка: Чернов Ярослав, Максим Стрелков
//------------------------------------------------------------------------

#ifndef FILEWATCHER_H
#define FILEWATCHER_H

#include <QObject>
#include <QFile>
#include <QFileInfo>
#include <QDateTime>
#include <QPixmap>
#include <QImage>

extern Q_CORE_EXPORT int qt_ntfs_permission_lookup;

class FileWatcher : public QObject
{
    Q_OBJECT
    QFileInfo* fileInfo;
    QDateTime* dTime;
    QString filename;
    QImage* image;

    int width;
    int height;
    int* intensity_X;
    int* intensity_Y;
    int maxIntensity_X;
    int maxIntensity_Y;
    int posX;
    int posY;
    int posX2;
    int posY2;
    int startPosX;
    int startPosY;
    int offsetX;
    int offsetY;
    bool isNotFirst;

public:
    explicit FileWatcher(QObject *parent = 0);

public slots:
    void Watch(QString filename);
    void FindCenter(QString filename);
    void FindCenter2(QString filename);

signals:
    void sigCenter(int, int);
    void sigCenter2(int, int, int, int);
};

#endif // FILEWATCHER_H
