#include "fitsfile.h"

FitsFile::FitsFile(QObject *parent) : QObject(parent)
{
    status = 0 ;
    fpixel = 1;
    naxis = 2;
    nelements = 0;
    exposure = 0;
    naxes[0] = 10;
    naxes[1] = 10;
}

int FitsFile::CreateFitsFile(const char * fitsname, uint32_t size, unsigned short * buf)
{
    array = new unsigned short [size] ;
    status = 0; /* initialize status before calling fitsio routines */
    fits_create_file(&fptr, fitsname, &status); /* create new file */
    /* Create the primary array image (16-bit short integer pixels */
    fits_create_img(fptr, USHORT_IMG, naxis, naxes, &status);
    /* Write a keyword; must pass the ADDRESS of the value */
    exposure = 1500.0;
    fits_update_key(fptr, TLONG, "EXPOSURE", &exposure,
    "Total Exposure Time", &status);
    /* Initialize the values in the image with a linear ramp function */
    for ( uint32_t i = 0; i < size; i++)
    {
        array[i] = buf[i];
    }
    nelements = size; /* number of pixels to write */
    /* Write the array of integers to the image */
    fits_write_img(fptr, TUSHORT, fpixel, nelements, array, &status);
    fits_close_file(fptr, &status); /* close the file */
    fits_report_error(stderr, status); /* print out any error messages */
    delete [] array;
    return( status );
}

