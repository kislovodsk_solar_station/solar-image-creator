#ifndef SOLARIMAGECREATORDLG_H
#define SOLARIMAGECREATORDLG_H

#include "imageTiff.h"
#include "filtertools.h"
#include "savetools.h"

#include <QDialog>
#include <QPixmap>
#include <QWidget>
#include <QLabel>
#include <QFileDialog>
#include <QPainter>
#include <QWheelEvent>

#include <stdint.h>

class MyLabel : public QLabel
{
public:
    MyLabel()
    {
        points1 = new QPoint;
        points2 = new QPoint;
        func1 = new QPoint;
        func2 = new QPoint;
        func3 = new QPoint;
        func4 = new QPoint;
        func5 = new QPoint;
        func6 = new QPoint;
        func7 = new QPoint;
        func8 = new QPoint;
        totalPoints1 = 0;
        totalPoints2 = 0;
        totalPointsfunc1 = 0;
        totalPointsfunc2 = 0;
        totalPointsfunc3 = 0;
        totalPointsfunc4 = 0;
        totalPointsfunc5 = 0;
        totalPointsfunc6 = 0;
        totalPointsfunc7 = 0;
        totalPointsfunc8 = 0;
        setScaledContents(true);
    }
    void PurgeFuncs()
    {
        totalPoints1 = 0;
        totalPoints2 = 0;
        totalPointsfunc1 = 0;
        totalPointsfunc2 = 0;
        totalPointsfunc3 = 0;
        totalPointsfunc4 = 0;
        totalPointsfunc5 = 0;
        totalPointsfunc6 = 0;
        totalPointsfunc7 = 0;
        totalPointsfunc8 = 0;
    }
    int x0;
    int y0;
    int x1;
    int y1;
    QPoint* points1;
    QPoint* points2;
    QPoint* func1;
    QPoint* func2;
    QPoint* func3;
    QPoint* func4;
    QPoint* func5;
    QPoint* func6;
    QPoint* func7;
    QPoint* func8;
    int totalPoints1;
    int totalPoints2;
    int totalPointsfunc1;
    int totalPointsfunc2;
    int totalPointsfunc3;
    int totalPointsfunc4;
    int totalPointsfunc5;
    int totalPointsfunc6;
    int totalPointsfunc7;
    int totalPointsfunc8;
protected:
    virtual void paintEvent(QPaintEvent* e);
    virtual void wheelEvent(QWheelEvent * event);
};



namespace Ui {
class SolarImageCreatorDlg;
}

class SolarImageCreatorDlg : public QDialog
{
    Q_OBJECT

public:
    explicit SolarImageCreatorDlg(QWidget *parent = 0);
    ~SolarImageCreatorDlg();
    static uint16_t number;
protected:
    void paintEvent(QPaintEvent* e);
    void resizeEvent(QResizeEvent * ev);
    void closeEvent(QCloseEvent * ev);
private:
    bool isSetPosition;
    QPoint* arr;
    QImage filteredImg;
    QImage* image;
    QString filename;
    ImageTiff* tiffimg;
    Ui::SolarImageCreatorDlg *ui;
    QImage* img1;
    QImage* img2;
    QImage* img3;
    QPixmap* pix;
    QPainter* painter;
    MyLabel* mylbl;
    FilterTools* fTools;
    SaveTools* sTools;
public slots:
    void ShowFilteredImage ( QPixmap pixmap,
                             QPoint*, int,
                             QPoint*, int,
                             QPoint*, int,
                             QPoint*, int,
                             QPoint*, int,
                             QPoint*, int,
                             QPoint*, int,
                             QPoint*, int,
                             QPoint*, int,
                             QPoint*, int);
    void initLimits( int x1, int x2, int y1, int y2);
    void SetFilterToolsVisible(bool b);
    void ShowImage ( QPixmap pixmap );
    void SetCentralLine( QPixmap pixmap,
                        QPoint* points, int totPoints,
                        QPoint* points1, int totPoints1 );
    void ShowSpectroFocus( QPixmap pixmap,
                           QPoint* points, int totPoints,
                           QPoint* points1, int totPoints1 );
signals:
    void sigTranspChanged(int);
    void sigPointsCountChanged(int);
    void sigLimx1Changed(int);
    void sigLimx2Changed(int);
    void sigLimy1Changed(int);
    void sigLimy2Changed(int);
    void sigContrastChanged(int);
    void sigSaveImage(QString, QString);
    void sigHorizontFiltChanged(bool);
    void sigVerticalFiltChanged(bool);
};


#endif // SOLARIMAGECREATORDLG_H
