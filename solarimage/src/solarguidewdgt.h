//------------------------------------------------------------------------
// Класс для вывода изображения Солнца и отметки центра солнечного диска
// Разработка: Чернов Ярослав, Максим Стрелков
//------------------------------------------------------------------------

#ifndef SOLARGUIDEWDGT_H
#define SOLARGUIDEWDGT_H

#include "filewatcher.h"
#include <QThread>
#include <QWidget>

namespace Ui {
class SolarGuideWdgt;
}

class SolarGuideWdgt : public QWidget
{
    Q_OBJECT
    Ui::SolarGuideWdgt *ui; // указатель на графические элементы класса
    FileWatcher* fileWatcher;
    QThread * thread;
    QString path;
    QImage* image;
    QPixmap* pixmap;

public:
    explicit SolarGuideWdgt(QWidget *parent = 0);
    ~SolarGuideWdgt();

public slots:
    void Open();
    void ShowImage2(int x1, int x2, int y1, int y2);
    void ShowImage(int x, int y);


signals:
    void openFile(QString );
};

#endif // SOLARGUIDEWDGT_H
