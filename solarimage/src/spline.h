//Cubic spline interpolation program
//when we have two columns of data x and y in input file:
//
//x0 y0
//x1 y1
//...
//xn yn
//
//and we want to find such function f(x)  
//where f(xi) = yi
//and f(x) is cubic function on every [x_k-1, x_k] segment
//and f(x), f'(x), f''(x) are continual
//the result is four columns of cubic polinom coefficients
#ifndef SPLINE_H
#define SPLINE_H

#include <math.h>
#include <stdio.h>
#include <process.h>

void FreeMatrix();
double GetSpline(double s);
void CreateSpline( double * x1, double * y1, int n );

#include <cstdlib>
#include <cmath>
#include <limits>

class cubic_spline
{
private:
    // Структура, описывающая сплайн на каждом сегменте сетки
    struct spline_tuple
    {
        double a, b, c, d, x;
    };

    spline_tuple *splines; // Сплайн
    std::size_t n; // Количество узлов сетки

    void free_mem(); // Освобождение памяти

public:
    cubic_spline(); //конструктор
    ~cubic_spline(); //деструктор

    // Построение сплайна
    // x - узлы сетки, должны быть упорядочены по возрастанию, кратные узлы запрещены
    // y - значения функции в узлах сетки
    // n - количество узлов сетки
    void build_spline(const double *x, const double *y, std::size_t n);

    // Вычисление значения интерполированной функции в произвольной точке
    double f(double x) const;
};




#endif // SPLINE_h
