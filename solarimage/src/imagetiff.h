#ifndef IMAGETIFF_H
#define IMAGETIFF_H

#include <QObject>

#include <tiffio.h>
#include <stdint.h>


class ImageTiff
{
    uint8_t* outbuffer; // Буфер для выходного TIFF изображения.
    int width;
    int height;
public:
    uint8_t* rawBuff; // Входной буфер для 2х uint8_t на пиксель.
    uint16_t* u16buff; // Входной буфер для 1го uint16_t на пиксель.
    uint8_t* u8buff; // Временный буфер для вывода 8 битного изображения на форму.

    ulong bytesCount;
    ulong rawBuffSize;
    ulong u16BuffSize;
    ulong u8BuffSize;

    explicit ImageTiff(const char * mfilename);
    explicit ImageTiff();
    ~ImageTiff();
    bool ReadTiff( const char * mfilename);
    void WriteTiff( const char * mfilename,
                    uint16_t* buf,
                    int m_width,
                    int m_height,
                    ulong m_bytes);
    void PurgeBuffer ();
    int Width(){ return width; }
    int Height(){ return height; }
    uint16_t Pixel(ulong n){ return u16buff[n]; }
    uint8_t Pixel8bit(ulong n){ return u8buff[n]; }
    ulong Size(){ return  bytesCount/2;}
//signals:

//public slots:
};

#endif // IMAGETIFF_H
