#include "solarimagecreator.h"
#include <QDebug>
#include <iostream>
#include <valarray>

#define SCAN_DIR "D:\Patrol spectraheliograph\Image\20160422_071240"
#define SCAN_IMG "C:/Projects/201509220444/obs01/sun.bmp"
#define UP_FILTER_LIMIT 40
#define READY isReady = true
#define NOT_READY isReady = false
#define TOTAL_FRAMES 1901

const QString imageFormats[25] = {
    "Invalid",
    "Mono",
    "MonoLSB",
    "Indexed8",
    "RGB32",
    "ARGB32",
    "ARGB32_Premultiplied",
    "RGB16",
    "ARGB8565_Premultiplied",
    "RGB666",
    "ARGB6666_Premultiplied",
    "RGB555",
    "ARGB8555_Premultiplied",
    "RGB888",
    "RGB444",
    "ARGB4444_Premultiplied",
    "RGBX8888",
    "RGBA8888",
    "RGBA8888_Premultiplied",
    "BGR30",
    "A2BGR30_Premultiplied",
    "RGB30",
    "A2RGB30_Premultiplied",
    "Alpha8",
    "Grayscale8"
};

//-----------------------------------------------------------------------------
//
//
SolarImageCreator::SolarImageCreator()
    :QObject()
{
    width = 0;
    height = 0;
    bytesCount = 0;
    wordsCount = 0;
    limx1 = 0;
    limx2 = 0;
    limy1 = 0;
    limy2 = 0;
    isCentralLineSet = false;
    pCount = 7;
    aprxPointsCount = 4;
    midPoints = new QPoint [10000];
    totalPoints = 0;
    midPoints1 = new QPoint [10000];
    totalPoints1 = 0;
    
    midPoints2 = new QPoint [10000];
    totalPoints2 = 0;
    
    mPts = new points_t[10000];
    
    vPoints = new QPoint [10000];
    vTotalPoints = 0;
    vPoints2 = new QPoint [10000];
    vTotalPoints2 = 0;
    vPoints3 = new QPoint [10000];
    vTotalPoints3 = 0;
    vPoints4 = new QPoint [10000];
    vTotalPoints4 = 0;
    
    hPoints = new QPoint [10000];
    hTotalPoints = 0;
    hPoints2 = new QPoint [10000];
    hTotalPoints2 = 0;
    hPoints3 = new QPoint [10000];
    hTotalPoints3 = 0;
    hPoints4 = new QPoint [10000];
    hTotalPoints4 = 0;
    
    transparency = 150;
    
    contrast = 100;
    isReady = true;
    isHorizontFilter = false;
    isVerticalFilter = true;
}
//-----------------------------------------------------------------------------
//
//
SolarImageCreator::~SolarImageCreator()
{
    
}

//-----------------------------------------------------------------------------
//
//
//
QString SolarImageCreator::FindLongestSpectorFile(QString path)
{
    QString fName;
    QString retName;
    QString tmp;
    ImageTiff tif;
    int32_t l = 0;
    bool isTifF = false;
    NOT_READY;
    emit sigStarted();
    // Определяем размер буфера для тифф изображения totalFrames * img.width()
    fName = path;
    fName += "0100.tif";
    tif.ReadTiff(fName.toStdString().c_str());
    //QImage img(imagePath);
    if ( tif.Size() == 0 )
    {
        fName = filename;
        fName += "0100.tiff";
        tif.ReadTiff(fName.toStdString().c_str());
        if ( tif.Size() == 0 )
        {
            qDebug() << "No image! ";
            emit sigFinished();
            emit sigWarningMessage ("Не могу открыть 0100.tif изображение");
            READY;
            return "";
        }
        isTifF = true;
    }

    for( int i = 0; i < TOTAL_FRAMES; i++)
    {
        fName = path ;
        if ( i < 10 )
            fName += "000";
        else if ( i < 100 )
            fName += "00";
        else if ( i < 1000 )
            fName += "0";
        fName += tmp.setNum(i);
        fName += isTifF ? ".tiff" : ".tif";
        tmp = "Подождите, идет поиск наибольшей линии спектра!\n";
        tmp.append( fName );
        emit sigProcessState(i/1.9, tmp );
        if ( !tif.ReadTiff((char*)fName.toStdString().c_str()) )
        {
            qDebug() << fName << " No image! ";
            continue;
        }
        // Устанавливаем значение, которое будет ограничивать
        // выбор пикселей.
        uint16_t r = 0x3700;
        // Предопределяем граничные координаты.
        limx1 = 0;
        limx2 = 0;//tif.Width() - 1;
        uint32_t av = 0;
        // Находим границы спектра.
        // Ищем приблизительную первую точку начала спектра слева.
        for ( int i = 0 ; i < tif.Width(); i++)
        {
            av = 0;
            for ( int j = 0; j < tif.Height(); j++)
            {
                av += tif.Pixel(j * tif.Width() + i);
            }
            av = av / tif.Height();
            // Получаем значение текущего пикселя.
            if ( av > r && limx1 == 0 )
            {
                // Как только найдем, выходим из поиска.
                limx1 = i;
                break;
            }
        }
        // Ищем последнюю приблезительную точку конца спекта справа.
        for ( int i = tif.Width() - 1 ; i > 0 ; i--)
        {
            av = 0;
            for ( int j = 0; j < tif.Height(); j++)
            {
                av += tif.Pixel(j * tif.Width() + i);
            }
            av = av / tif.Height();
            // Получаем значение текущего пикселя.
            if ( av > r && limx1 != 0 )
            {
                // Как только найдем, выходим из поиска.
                limx2 = i;
                break;
            }
        }
        if ( limx2 - limx1 > l)
        {
            l = limx2 - limx1;
            retName = fName;
        }
    }

    qDebug() << "l = " << l;
    qDebug() <<"longest spectro : " << retName;
    READY;
    emit sigFinished();
    return retName;
}

//-----------------------------------------------------------------------------
//
// SetCentralLine
//
// Функция используется для программной установки координат пикселей на
// спектре, по которым будет собираться общее изображение.
// Устанавливается на самом длинном изображении спектра.
//
void SolarImageCreator::SetCentralLine( QString m_filename, int offset )
{
    totalPoints1 = 0;
    totalPoints2 = 0;
    vTotalPoints = 0;
    vTotalPoints2 = 0;
    vTotalPoints3 = 0;
    vTotalPoints4 = 0;
    hTotalPoints = 0;
    hTotalPoints2 = 0;
    hTotalPoints3 = 0;
    hTotalPoints4 = 0;
    // Открываем tiff изображение. Глубина будет 8 бит. Но этого достаточно.
    // Сначала надо найти точки, по которым будем интерполировать и
    // экстраполировать кривую ( среднюю линию спектра ).
    isCentralLineSet = true;
    //QImage img(m_filename);
    ImageTiff tif(m_filename.toStdString().c_str());
    totalPoints = tif.Width();
    // Устанавливаем значение, которое будет ограничивать
    // выбор пикселей.
    uint16_t r = 0x3700;
    // Предопределяем граничные координаты.
    limx1 = 0;
    limx2 = tif.Width() - 1;
    uint32_t av = 0;
    // Находим границы спектра.
    // Ищем приблизительную первую точку начала спектра слева.
    for ( int i = 0 ; i < tif.Width(); i++)
    {
        av = 0;
        for ( int j = 0; j < tif.Height(); j++)
        {
            av += tif.Pixel(j * tif.Width() + i);
        }
        av = av / tif.Height();
        // Получаем значение текущего пикселя.
        if ( av > r && limx1 == 0 )
        {
            // Как только найдем, выходим из поиска.
            limx1 = i;
            break;
        }
    }
    // Ищем последнюю приблезительную точку конца спекта справа.
    for ( int i = tif.Width() - 1 ; i > 0 ; i--)
    {
        av = 0;
        for ( int j = 0; j < tif.Height(); j++)
        {
            av += tif.Pixel(j * tif.Width() + i);
        }
        av = av / tif.Height();
        // Получаем значение текущего пикселя.
        if ( av > r && limx1 != 0 )
        {
            // Как только найдем, выходим из поиска.
            limx2 = i;
            break;
        }
    }
    qDebug() << "limx1 = " << limx1 << " . " << "limx2 = " << limx2;

    int y1 = 0;
    //    int y2 = 0;
    //    int l = 0;
    // По каждому столбцу идет окно, ищем средние значения с последующим смещением окна по вертикали.
    // Сохраняем значение номера строки.
    // Как только средние значения начнут увеличиваться, значит центр линии пройден.
    // Точка центральной линии будет M_COUNT / 2.
#define M_COUNT 20
    uint16_t m[M_COUNT]; // Окно, в котором будут подсчитываться средние значения.

    for ( long i = 0 ; i < tif.Width() ; i++ )
    {
        r = 0xFFFF;
        for ( long j = UP_FILTER_LIMIT; j < tif.Height() - UP_FILTER_LIMIT; j++)
        {
            av = 0;
            //uint16_t rgb = tif.Pixel(j * tif.Width() + i);
            for( int j1 = 0; j1 < M_COUNT; j1++)
            {
                m[j1] = tif.Pixel((j + j1) * tif.Width() + i);
                av += m[j1];
            }
            av /= M_COUNT;
            if ( av < r )
            {
                r = av;
                y1 = j + M_COUNT / 2;
            }
        }

        midPoints[i].setY(y1);
        hPoints[i].setY(y1);
        midPoints[i].setX(i);
        hPoints[i].setX(i);
        
        
        //        for ( long i = 0 ; i < tif.Width() ; i++ )
        //        {
        //            for ( long j = UP_FILTER_LIMIT; j < tif.Height() - UP_FILTER_LIMIT; j++)
        //            {
        //                //uint16_t rgb = tif.Pixel(j * tif.Width() + i);
        //    //            if ( rgb < r && y1 == 0 )
        //    //            {
        //    //                //qDebug() << j << ". " << hex << rgb;
        //    //                y1 = j;
        //    //            }
        //    //            else if ( rgb > r && y1 != 0 )
        //    //            {
        //    //                y2 = j;
        //    //                break;
        //    //            }
        //            }
        //            //if ( y2 - y1 == 0 )
        //            //    midPoints[i].setY(midPoints[i-1].y());
        //            //qDebug() << " x = " << i << " l = " << y2 - y1 ;
        //            //l = y1 + (y2 - y1)/2;
        //            //midPoints[i].setY(l);
        //            //hPoints[i].setY(l);
        //            //midPoints[i].setX(i);
        //            //hPoints[i].setX(i);
        //            //y1 = 0;
        //            //y2 = 0;
        
        
        
        //        if ( limx1 == 0 && limx2 == tif.Width() - 1)
        //        {
        //            midPoints[i].setX(i);
        //            midPoints[i].setY(tif.Height() / 2);
        //        }
    }
    
    
    
    // Начало и конец спектра найдены. Теперь надо искать
    // положение линии поглощения по вертикали.

    r = 0xFFFF;
    // Перебираем массив пикселей с верхним и нижним ограничением.
    // Ищем пиксели по каждому столбцу с минимальной интенсивностью.
    
    //    for ( long i = 0 ; i < tif.Width() ; i++ )
    //    {
    //        for ( long j = UP_FILTER_LIMIT; j < tif.Height() - UP_FILTER_LIMIT; j++)
    //        {
    //            uint16_t rgb = tif.Pixel(j * tif.Width() + i);
    //            if ( rgb < r)
    //            {
    //                r = rgb;
    //                midPoints[i].setY(j);
    //                hPoints[i].setY(j);
    //            }
    //        }
    //        midPoints[i].setX(i);
    //        hPoints[i].setX(i);
    //        r = 0xFFFF;
    //        if ( limx1 == 0 && limx2 == tif.Width() - 1)
    //        {
    //            midPoints[i].setX(i);
    //            midPoints[i].setY(tif.Height() / 2);
    //        }
    //    }
    //    qDebug() << "limx1 = " << limx1 << " limx2 = " << limx2;
    
    double * arrx = new double[aprxPointsCount];
    double * arry = new double[aprxPointsCount];
    
    
    arrx[0] = limx1;
    arry[0] = midPoints[limx1].y();
    for ( int i = 1 ; i < aprxPointsCount ; i++)
    {
        arrx[i] = limx1 + (limx2 - limx1) / (aprxPointsCount - 1) * (i);
        arry[i] = midPoints[(int)arrx[i]].y();
    }
    arrx[aprxPointsCount - 1] = limx2;
    arry[aprxPointsCount - 1] = midPoints[limx2].y();
    
    points1 = new QPoint[aprxPointsCount];
    totalPoints1 = aprxPointsCount;
    //uint16_t tmpval = 0;
    for ( int i = 0 ; i < aprxPointsCount; i++ )
    {
        points1[i].setX((int)arrx[i]);
        points1[i].setY((int)arry[i]);
        
    }
    
    //CreateSpline(arrx, arry, aprxPointsCount);
    spline.build_spline(arrx, arry, aprxPointsCount);
    // Аппроксимируем функцию.
    for ( int i = 0; i < tif.Width(); i++ )
    {
        //double t = Lagranj( i, arrx, arry , aprxPointsCount );
        //double t = GetSpline(i);
        double t = spline.f(i);
        //qDebug () << i << " : " << t;
        if ( t > tif.Height())
            t = tif.Height() - 1;
        if ( t < 0 )
            t = 0;
        midPoints[i].setY(t + offset);
        mPts[i].x = i;
        mPts[i].y = t + offset;
    }
    //FreeMatrix();
    //  Сохранение промежуточного результата.
    //  img.setPixel(x, y, 0xFF00FF00);
    //  QString str = mfilename;
    //  int n1 = str.indexOf(".tif");
    //  str.replace(n1, 4, "f.tif");
    //  img.save(str, "tif", 100);
    QPixmap pix2;
    QImage img2(tif.u8buff,
                tif.Width(),
                tif.Height(),
                tif.Width(),
                QImage::Format_Grayscale8 );
    pix2.convertFromImage(img2);
    delete [] arrx;
    delete [] arry;
    //delete [] points1;
    emit sigFinished();
    //emit sigCentralLineReady(pix2, midPoints, totalPoints, hPoints, totalPoints );
    emit sigCentralLineReady(pix2, midPoints, totalPoints, points1, aprxPointsCount );
}

//-----------------------------------------------------------------------------
//
//  Сборка спектрогелиорграммы
//  Добавить вычисление totalFrames!
//
void SolarImageCreator::CreateImage()
{
    QString imagePath = filename;
    uint totalFrames = 1900;
    QString tmp, str ;
    ulong totalBytes = 0;
    ulong iByte = 0;
    ulong iColumn = 0;
    NOT_READY;
    bool isTifF = false;
    if ( bytesCount > 0 || wordsCount > 0)
    {
        delete [] bytesBuff;
        delete [] wordsBuff;
        bytesCount = 0;
        wordsCount = 0;
        width = 0;
        height = 0;
    }
    
    if ( imagePath == NULL)
    {
        qDebug() << "filename == NULL";
        return;
    }
    // Определяем размер буфера для тифф изображения totalFrames * img.width()
    imagePath = filename;
    imagePath += "0100.tif";
    ImageTiff tif(imagePath.toStdString().c_str());
    //QImage img(imagePath);
    if ( tif.Size() == 0 )
    {
        imagePath = filename;
        imagePath += "0100.tiff";
        tif.ReadTiff(imagePath.toStdString().c_str());
        if ( tif.Size() == 0 )
        {
            qDebug() << "No image! ";
            emit sigWarningMessage ("Не могу открыть 0100.tif изображение");
            READY;
            return;
        }
        isTifF = true;
    }

    iByte = 0;
    emit sigStarted();
    emit sigProcessState(0, "Подождите, идет определение числа кадров!");
    width = tif.Width();
    totalFrames = 0;
    for ( uint i = 0 ; i < TOTAL_FRAMES; i++)
    {
        imagePath = filename ;
        if ( i < 10 )
            imagePath += "000";
        else if ( i < 100 )
            imagePath += "00";
        else if ( i < 1000 )
            imagePath += "0";
        imagePath += tmp.setNum(i);
        imagePath += isTifF ? ".tiff" : ".tif";
        tmp = "Подождите, идет определение числа кадров!\n";
        tmp.append( imagePath );
        emit sigProcessState(i/1.9, tmp );
        if ( !tif.ReadTiff((char*)imagePath.toStdString().c_str()) )
        {
            //qDebug() << imagePath << " No image! ";
            continue;
        }
        totalFrames++;
    }
    totalBytes = totalFrames * tif.Width();
    qDebug() << "Total Frames = " << totalFrames;
    qDebug() << "Total bytes = " << totalBytes;
    
    //    if( tiffimg->bytesCount != 0)
    //    {
    //        tiffimg->PurgeBuffer();
    //    }
    bytesCount = totalBytes;
    wordsCount = totalBytes;
    bytesBuff = new uint8_t [bytesCount];
    wordsBuff = new uint16_t[wordsCount];
    for ( uint i = 0 ; i < TOTAL_FRAMES; i++)
    {
        imagePath = filename ;
        if ( i < 10 )
            imagePath += "000";
        else if ( i < 100 )
            imagePath += "00";
        else if ( i < 1000 )
            imagePath += "0";
        imagePath += tmp.setNum(i);
        imagePath += isTifF ? ".tiff" : ".tif";
        //qDebug() << imagePath;
        tmp = "Подождите, идет сборка спектрогелеограммы!\n";
        tmp.append( imagePath );
        emit sigProcessState(i/1.9, tmp );
        
        if ( !tif.ReadTiff((char*)imagePath.toStdString().c_str()))
        {
            qDebug() << imagePath << " No image! ";
            continue;
        }
        //thread->usleep(1000000);
        //SetCentralLine(imagePath, 0);
        for ( int j = 0 ; j < tif.Width(); j++ )
        {
            iByte = j  * totalFrames + iColumn;
            int n = 0;
            wordsBuff[iByte] = 0;
            uint32_t tmp =  mPts[j].y;
            double dtmp = 0;
            dtmp = mPts[j].y - tmp;
            dtmp = dtmp * tif.Pixel(( tmp + 1 + n) * tif.Width() + j);
            //dtmp1 = dtmp1 * dtmp0;
            wordsBuff[iByte] = dtmp;
            dtmp = 1 - mPts[j].y + tmp;
            dtmp = dtmp * tif.Pixel((tmp + n) * tif.Width() + j);
            //dtmp1 = dtmp1 * dtmp0;
            wordsBuff[iByte] += dtmp;
            bytesBuff[iByte] = wordsBuff[iByte] / 256;
        }
        iColumn++;
    }
    height = width;
    width = totalFrames ;
    imagePath = filename;
    imagePath += "SunMid.tif";
    
    emit sigFinished();
    READY;
    qDebug () << width << " "
              << height<< " "
              << totalBytes << " "
              << totalFrames << " "
              << iColumn;
    QImage img2(bytesBuff,
                width,
                height,
                width,
                QImage::Format_Grayscale8 );
    QPixmap pix2;
    pix2.convertFromImage(img2);
    emit sigImageReady(pix2);
    return;
    
}

//-----------------------------------------------------------------------------
//
//      Интерполяция полиномом Лагранджа.
//
double SolarImageCreator::Lagranj (double X, double * x, double* y, int size )
{
    double L = 0;
    double l;
    for (int i = 0; i < size; i++)
    {
        l = 1;
        for ( int j = 0; j < size; j++)
        {
            if (i != j)
                l *= (X - x[j]) / (x[i] - x[j]);
        }
        L += y[i] * l;
    }
    return L;
}


////-----------------------------------------------------------------------------
////
////
//FilteringImageDlg::FilteringImageDlg(QWidget *parent) :
//    QWidget(parent),
//    ui(new Ui::FilteringImageDlg)
//{
//    ui->setupUi(this);


//}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreator::FilterImage( )
{
    qDebug() << "FilterImage " << filename;
    emit sigStarted();
    emit sigProcessState(0, "Подождите, идет обработка!");
    NOT_READY;
    if ( bytesCount > 0 || wordsCount > 0)
    {
        delete [] bytesBuff;
        delete [] wordsBuff;
        bytesCount = 0;
        wordsCount = 0;
        width = 0;
        height = 0;
    }
    if ( filename.endsWith(".jpeg") || filename.endsWith(".jpg"))
    {
        qDebug() << "JPG!!!";
        QImage img(filename);
        qDebug()<< "format = " << img.format() << " "
                << imageFormats[img.format()];
        qDebug()<< "bytes count = " << img.byteCount();
        qDebug()<< "bytes per line = " << img.bytesPerLine();
        qDebug()<< "width = " << img.width();
        qDebug()<< "height = " << img.height();
        if ( img.format() != QImage::Format_Grayscale8)
        {
            QString str = "Формат JPG ";
            str.append(imageFormats[img.format()]);
            str.append(" не поддерживается");
            emit sigWarningMessage(str);
            READY;
            emit sigFinished();
            return;
        }
        width = img.width() + 1;// Почему-то bytesPerLine() определяет размер на 1 больше
        height = img.height();
        bytesCount = img.byteCount();
        wordsCount = bytesCount;
        bytesBuff = new uint8_t[bytesCount];
        wordsBuff = new uint16_t[wordsCount];
        for ( ulong i = 0; i < bytesCount; i++)
        {
            bytesBuff[i] = img.bits()[i];
            wordsBuff[i] = bytesBuff[i] * 256;
        }
        
    }
    else if ( filename.endsWith(".tif"))
    {
        ImageTiff tif(filename.toStdString().c_str());
        width = tif.Width();
        height = tif.Height();
        bytesCount = tif.Size();
        wordsCount = tif.Size();
        bytesBuff = new uint8_t[bytesCount];
        wordsBuff = new uint16_t[wordsCount];
        for ( ulong i = 0; i < bytesCount; i++)
            bytesBuff[i] = tif.Pixel8bit(i);
        for ( ulong i = 0; i < wordsCount; i++)
            wordsBuff[i] = tif.Pixel(i);
    }
    static bool isLimInited = false;
    if ( isLimInited == false )
    {
        isLimInited = true;
        limx1 = 0;
        limx2 = width - 1;
        limy1 = 0;
        limy2 = height - 1;
        emit sigInitLimits ( limx1, limx2, limy1, limy2);
    }
    totalPoints1 = 0;
    hTotalPoints = 0;
    hTotalPoints2 = 0;
    hTotalPoints3 = 0;
    hTotalPoints4 = 0;
    totalPoints2 = 0;
    vTotalPoints = 0;
    vTotalPoints2 = 0;
    vTotalPoints3 = 0;
    vTotalPoints4 = 0;
    
    for ( int i = 0; i < pCount; i++ )
    {
        midPoints1[i].setX(0);
        midPoints1[i].setY(0);
        midPoints2[i].setX(0);
        midPoints2[i].setY(0);
    }
    int32_t * hAvaragesArr = new int32_t [width];
    int32_t * hInterpolateArr = new int32_t [width];
    int32_t * hResultArr = new int32_t [width];
    double * arrx = new double[width];
    double * arry = new double[width];
    ulong tmp = 0;
    
    if ( isVerticalFilter == true )
    {
        totalPoints1 = width;
        hTotalPoints = width;
        hTotalPoints2 = width;
        hTotalPoints3 = width;
        hTotalPoints4 = width;
        // Находим средние значения по столбцам.
        
        for ( int x = 0; x < width; x++ )
        {
            for ( int y = 0 ; y < height; y++ )
            {
                tmp += wordsBuff[y * width + x];
            }
            tmp = tmp / height;
            hPoints2[x].setX(x);
            hPoints2[x].setY(0);
            hPoints3[x].setX(x);
            hPoints2[x].setY(0);
            hPoints[x].setX(x);
            hPoints[x].setY(tmp/256);
            hAvaragesArr[x] = tmp;
            tmp = 0;
        }
        
        // Находим точки интерполяции. Первая и последняя точка очень важны.
        //arrx[0] = 0;
        //arry[0] = hPoints[0].y();
        arrx[0] = limx1;
        arry[0] = hAvaragesArr[limx1];
        qDebug () << arrx[0]  << "." << arry[0];
        for ( int i = 1 ; i < pCount ; i++)
        {
            arrx[i] = limx1 + (limx2 - limx1) / (pCount - 1) * (i);
            arry[i] = hAvaragesArr[(int)arrx[i]];
            qDebug () << arrx[i]  << "." << arry[i];
        }
        arrx[pCount - 1] = limx2;
        arry[pCount - 1] = hAvaragesArr[limx2];
        //arrx[pCount - 1] = img.width() - 1;
        //arry[pCount - 1] = hPoints[img.width() - 1].y();
        
        for ( int i = 0; i < pCount; i++)
        {
            midPoints1[i].setX(arrx[i]);
            midPoints1[i].setY(arry[i]/256);
        }
        
        // Аппроксимируем функцию.
        spline.build_spline(arrx, arry, pCount);
        for ( int x = 0; x < width; x++ )
        {
            hInterpolateArr[x] =  Lagranj( x, arrx, arry , pCount );
            hInterpolateArr[x] = spline.f(x);
            if ( hInterpolateArr[x] < 0 )
                hInterpolateArr[x] = 0;
            hPoints2[x].setY( hInterpolateArr[x] / 256 );
        }
        delete [] arrx;
        delete [] arry;
        //int hmin = 0;
        for ( int x = 0 ; x < totalPoints1 ; x++)
        {
            //if ( hAvaragesArr[x] - hInterpolateArr[x] < hmin )
            //    hmin = hAvaragesArr[x] - hInterpolateArr[x] ;
            hResultArr[x] = ( hAvaragesArr[x] - hInterpolateArr[x]) ;
            //qDebug () << hResultArr[x];
        }
        for ( int x = 0 ; x < totalPoints1 ; x++)
        {
            //hResultArr[x] =  hResultArr[x] + (-1) * hmin;
            //hResultArr[x] += -hmin;
            //hResultArr[x] *= -1;
            
            //tmpImg.setPixel(x, hPoints3[x].y(), 0xFFFF7F00);
            hPoints3[x].setY(( hResultArr[x] / 256 ));
            hPoints3[x].setX(x);
        }
    }
    
    // Фильтрация по горизонтали
    int32_t * vResultArr = new int32_t [height];
    int32_t * vInterpolateArr = new int32_t [height];
    // Находим средние значения по строкам.
    int32_t* vAvaragesArr = new int32_t [height];
    
    if ( isHorizontFilter == true )
    {
        totalPoints2 = height;
        vTotalPoints = height;
        vTotalPoints2 = height;
        vTotalPoints3 = height;
        vTotalPoints4 = height;
        for ( int y = 0; y < height; y++ )
        {
            for ( int x = 0 ; x < width; x++ )
            {
                tmp += wordsBuff[y * width + x];
            }
            tmp = tmp / width;
            vPoints2[y].setX(0);
            vPoints2[y].setY(y);
            vPoints3[y].setX(0);
            vPoints2[y].setY(y);
            vPoints[y].setX(tmp/256);
            vPoints[y].setY(y);
            vAvaragesArr[y] = tmp;
            tmp = 0;
        }
        // Аппроксимация по строкам.
        arrx = new double[height];
        arry = new double[height];
        arry[0] = limy1;
        arrx[0] = vAvaragesArr[limy1];
        qDebug () << arrx[0]  << "." << arry[0];
        for ( int i = 1 ; i < pCount ; i++)
        {
            arry[i] = limy1 + (limy2 - limy1) / (pCount - 1) * (i);
            arrx[i] = vAvaragesArr[(int)arry[i]];
            qDebug () << arrx[i]  << "." << arry[i];
        }
        arry[pCount - 1] = limy2;
        arrx[pCount - 1] = vAvaragesArr[limy2];
        
        for ( int i = 0; i < pCount; i++)
        {
            midPoints2[i].setX(arrx[i]/256);
            midPoints2[i].setY(arry[i]);
        }
        
        // Аппроксимируем функцию.
        for ( int y = 0; y < height; y++ )
        {
            vInterpolateArr[y] =  Lagranj( y, arry, arrx , pCount );
            if ( vInterpolateArr[y] < 0 )
                vInterpolateArr[y] = 0;
            vPoints2[y].setX( vInterpolateArr[y] / 256 );
        }
        delete [] arrx;
        delete [] arry;
        //int vmin = 0;
        for ( int y = 0 ; y < totalPoints2 ; y++)
        {
            //if ( vAvaragesArr[y] - vInterpolateArr[y] < vmin )
            //    vmin = vAvaragesArr[y] - vInterpolateArr[y] ;
            vResultArr[y] = ( vAvaragesArr[y] - vInterpolateArr[y]) ;
        }
        for ( int y = 0 ; y < totalPoints2 ; y++)
        {
            // vResultArr[y] =  vResultArr[y] + (-1) * vmin;
            //tmpImg.setPixel(x, hPoints3[x].y(), 0xFFFF7F00);
            vPoints3[y].setX(( vResultArr[y] / 256 ));
            vPoints3[y].setY(y);
        }
    }
    
    // Собираем фильтрующее изображение.
    int16_t * filtermap = new int16_t [width * height];
    int32_t * dfiltermap = new int32_t [width * height];
    int z = 0;
    int32_t temp = 0;
    for ( int y = 0 ; y < height ; y++)
    {
        for ( int x = 0 ; x < width; x++)
        {
            if ( isHorizontFilter == true && isVerticalFilter == true )
            {
                temp = hResultArr[x] + vResultArr[y];
            }
            else if ( isHorizontFilter == false && isVerticalFilter == true )
            {
                temp = hResultArr[x];
            }
            else if ( isHorizontFilter == true && isVerticalFilter == false )
            {
                temp = vResultArr[y];
            }
            else if ( isHorizontFilter == false && isVerticalFilter == false )
            {
                temp = 0;
            }
            
            if ( temp > 0xFFFF)
            {
                temp = 0xFFFF;
            }
            filtermap[z] = temp / 256;
            dfiltermap[z] = temp ;
            z++;
        }
    }
    // Сохраняем фильтрующее изображение.
    //    img2 = new QImage(filtermap,
    //                      tiffimg->Width(),
    //                      tiffimg->Height(),
    //                      QImage::Format_Grayscale8 );
    //    QString str = filename;
    //    str.replace( str.indexOf(".tif"), 4, "_mask" );
    //    str.append("_");
    //    //str.append(ui->spinBoxTransp->text());
    //    str.append(".tif");
    //    img2->save(str);
    temp = 0;
    for ( int32_t z = 0 ; z < width * height ; z++)
    {
        temp = ( wordsBuff[z] * (filtermap[z])) / transparency;
        //temp =  tiffimg->u16buff[z] - dfiltermap[z];
        temp = wordsBuff[z] - temp;
        temp *= contrast / 100;
        //temp = dfiltermap[z];
        if ( temp > 0xFFFF )
        {
            wordsBuff[z] = 0xFFFF;
        }
        else if ( temp < 0 )
        {
            temp = 0;
        }
        else
            wordsBuff[z] = temp ;
        bytesBuff[z] = wordsBuff[z] / 256;
    }
    QImage img2(bytesBuff,
                width,
                height,
                width,
                QImage::Format_Grayscale8 );
    QPixmap pix2;
    pix2.convertFromImage(img2);
    
    // Опять находим средние значения по столбцам.
    tmp = 0;
    for ( int x = 0; x < width; x++ )
    {
        for ( int y = 0 ; y < height; y++ )
        {
            tmp += wordsBuff[y * width + x];
        }
        tmp = tmp / height;
        hPoints4[x].setX(x);
        hPoints4[x].setY(tmp / 256);
        tmp = 0;
    }
    
    tmp = 0;
    for ( int y = 0 ; y < height; y++ )
    {
        for ( int x = 0; x < width; x++ )
        {
            tmp += wordsBuff[y * width + x];
        }
        tmp = tmp / width;
        vPoints4[y].setX(tmp / 256);
        vPoints4[y].setY(y);
        tmp = 0;
    }
    
    delete [] hAvaragesArr;
    delete [] hInterpolateArr;
    delete [] hResultArr;
    
    delete [] vAvaragesArr;
    delete [] vResultArr;
    delete [] vInterpolateArr;
    delete [] filtermap;
    delete [] dfiltermap;
    //qDebug() << "Point 1";
    emit sigFilteredImageReady( pix2,
                                hPoints,
                                hTotalPoints,
                                hPoints2,
                                hTotalPoints2,
                                hPoints3,
                                hTotalPoints3,
                                hPoints4,
                                hTotalPoints4,
                                vPoints,
                                vTotalPoints,
                                vPoints2,
                                vTotalPoints2,
                                vPoints3,
                                vTotalPoints3,
                                vPoints4,
                                vTotalPoints4,
                                midPoints1,
                                pCount,
                                midPoints2,
                                pCount
                                );
    READY;
    emit sigFinished();
}

//-----------------------------------------------------------------------------
//
//
//
void SolarImageCreator::CalcSpectroFocus()
{
    //    emit sigStarted();
    //    emit sigProcessState(0, "Подождите, идет обработка!");
    //    isReady = false;
    //    if( tiffimg->bytesCount != 0)
    //    {
    //        tiffimg->PurgeBuffer();
    //    }
    //    tiffimg->ReadTiff((char*)filename.toStdString().c_str());
    
    //    //    for ( int i = 0; i < pCount; i++ )
    //    //    {
    //    //        midPoints1[i].setX(0);
    //    //        midPoints1[i].setY(0);
    //    //        midPoints2[i].setX(0);
    //    //        midPoints2[i].setY(0);
    //    //    }
    
    //    ulong tmp = 0;
    //    uint pointAtX = tiffimg->Width() / 2;
    //    uint d = pointAtX -  tiffimg->u16buff[ pointAtX ] / 256;
    //    vTotalPoints = tiffimg->Height();
    //    vTotalPoints2 = tiffimg->Height();
    //    // Находим средние значения по строкам.
    
    //    for ( int y = 0; y < tiffimg->Height(); y++ )
    //    {
    //        //for ( int x = 0; x < tiffimg->Width(); x++ )
    //        //{
    //        //    tmp += tiffimg->u16buff[y * tiffimg->Width() + x];
    //        //}
    //        //tmp = tmp / tiffimg->Width();
    //        tmp = tiffimg->u16buff[y * tiffimg->Width() + pointAtX];
    //        vPoints[y].setX(tmp/256 + d);
    //        vPoints[y].setY(y);
    //        vPoints2[y].setX(tiffimg->Width() / 2);
    //        vPoints2[y].setY(y);
    //        tmp = 0;
    //    }
    //    img1 = new QImage(tiffimg->u8buff,
    //                      tiffimg->Width(),
    //                      tiffimg->Height(),
    //                      QImage::Format_Grayscale8 );
    
    
    //    *pix = QPixmap::fromImage(*img1);
    //    emit sigShowSpectroFocus( *pix,
    //                              vPoints,
    //                              vTotalPoints,
    //                              vPoints2,
    //                              vTotalPoints2
    //                              );
    //    //delete img2;
    //    delete img1;
    
    //    isReady = true;
    //    emit sigFinished();
}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreator::TranspChanged(int val)
{
    //qDebug() << "onTranspChanged " << val;
    transparency = val;
    if ( isReady )
    {
        FilterImage();
    }
}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreator::PointsCountChanged(int val)
{
    pCount = val;
    if ( isReady )
    {
        FilterImage();
    }
    
}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreator::onLimx1Changed(int val)
{
    limx1 = val;
    if ( isReady )
    {
        FilterImage();
    }
}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreator::onLimx2Changed(int val)
{
    limx2 = val;
    if ( isReady )
    {
        FilterImage();
    }
}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreator::onLimy1Changed(int val)
{
    limy1 = val;
    if ( isReady )
    {
        FilterImage();
    }
}
void SolarImageCreator::onLimy2Changed(int val)
{
    limy2 = val;
    if ( isReady )
    {
        FilterImage();
    }
}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreator::onContrastChanged(int val)
{
    contrast = val;
    if ( isReady )
    {
        FilterImage();
    }
}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreator::onSaveImage(QString m_filename, QString type)
{
    //if ( isReady )
    {
        if ( type.compare("tif") == 0)
        {
            
            QString str = m_filename;
            //str.replace( str.indexOf(".tif"), 4, "_filt" );
            //str.append(".tif");
            ImageTiff tif;
            tif.WriteTiff(str.toStdString().c_str(),
                          wordsBuff,
                          width,
                          height,
                          wordsCount);
            qDebug() <<"write " <<  type;
        }
        else if ( type.compare("jpg") == 0)
        {
            QImage img2(bytesBuff,
                        width,
                        height,
                        width,
                        QImage::Format_Grayscale8 );
            img2.save(m_filename);
            qDebug() <<"write " <<  type;
        }
        else if ( type.compare("fit") == 0)
        {
            FitsFile fits;
            fits.naxes[0] = width;
            fits.naxes[1] = height;
            fits.CreateFitsFile( m_filename.toStdString().c_str(),
                                 wordsCount,
                                 (unsigned short*)wordsBuff);
            qDebug() <<"write " <<  type;
        }
    }
}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreator::onHorizontFiltChanged(bool checked)
{
    isHorizontFilter = checked;
    if ( isReady )
    {
        FilterImage();
    }
}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreator::onVerticalFiltChanged(bool checked)
{
    isVerticalFilter = checked;
    if ( isReady )
    {
        FilterImage();
    }
}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreator::CreateDoplerImage2()
{
    QString fName = FindLongestSpectorFile(filename);
    if ( fName.isNull())
        return;
    SetCentralLine(fName, 6);
    CreateImage();
    int32_t * doplerBuf = new int32_t [wordsCount];
    int32_t maxVal = 0;
    int32_t minVal = 0;
    for ( int i = 0 ; i < wordsCount; i++)
    {
        doplerBuf[i] = wordsBuff[i];
    }
    SetCentralLine(fName, -6);
    CreateImage();

    for ( uint i = 0 ; i < wordsCount; i++ )
    {
        doplerBuf[i] = doplerBuf[i] - wordsBuff[i];

        //doplerBuf[j] += 0x7FFF;
        if ( doplerBuf[i] < minVal)
            minVal = doplerBuf[i];
        if ( doplerBuf[i] > maxVal)
            maxVal = doplerBuf[i];

        //doplerBuf[j] *= 3;
        //wordsBuff[j] = doplerBuf[j];
        //bytesBuff[j] = wordsBuff[j] / 256;
    }
    qDebug() << "minVal = " << minVal;
    qDebug() << "maxVal = " << maxVal;

    int32_t maxVal2 = 0;
    float k = maxVal + -minVal;
    k = 0xFFFF/k;
    for ( uint i = 0 ; i < wordsCount; i++ )
    {
        doplerBuf[i] += -minVal;
        wordsBuff[i] = doplerBuf[i] * k ;
        bytesBuff[i] = wordsBuff[i] / 256;
        if ( maxVal2 < wordsBuff[i])
            maxVal2 = wordsBuff[i];
    }
    qDebug() << "maxVal2 = " << maxVal2;

    QImage img2(bytesBuff,
                width,
                height,
                width,
                QImage::Format_Grayscale8 );
    QPixmap pix2;
    pix2.convertFromImage(img2);
    emit sigImageReady(pix2);
    emit sigFinished();
    delete [] doplerBuf;
    READY;
}

//-----------------------------------------------------------------------------
//
//
void SolarImageCreator::CreateDoplerImage()
{
    QString imagePath = filename;
    uint totalFrames = 0;
    QString tmp, str ;
    ulong totalBytes = 0;
    ulong iByte = 0;
    ulong iColumn = 0;
    NOT_READY;
    
    if ( bytesCount > 0 || wordsCount > 0)
    {
        delete [] bytesBuff;
        delete [] wordsBuff;
        bytesCount = 0;
        wordsCount = 0;
        width = 0;
        height = 0;
    }
    
    if ( imagePath == NULL)
    {
        qDebug() << "filename == NULL";
        return;
    }
    // Определяем размер буфера для тифф изображения totalFrames * img.width()
    imagePath = filename;
    imagePath += "0100.tif";
    //QImage img(imagePath);
    ImageTiff tif(imagePath.toStdString().c_str());
    if ( tif.Size() == 0 )
    {
        qDebug() << "No image! ";
        emit sigWarningMessage ("Не могу открыть 0100.tif изображение");
        READY;
        return;
    }
    iByte = 0;
    emit sigStarted();
    emit sigProcessState(0, "Подождите, идет обработка!");
    width = tif.Width();
    totalFrames = 0;
    for ( uint i = 0 ; i < TOTAL_FRAMES; i++)
    {
        imagePath = filename ;
        if ( i < 10 )
            imagePath += "000";
        else if ( i < 100 )
            imagePath += "00";
        else if ( i < 1000 )
            imagePath += "0";
        imagePath += tmp.setNum(i);
        imagePath += ".tif";
        if ( !tif.ReadTiff((char*)imagePath.toStdString().c_str())  )
        {
            continue;
        }
        totalFrames++;
    }
    totalBytes = totalFrames * tif.Width();
    qDebug() << "Total Frames = " << totalFrames;
    qDebug() << "Total bytes = " << totalBytes;
    emit sigStarted();
    emit sigProcessState(0, "Подождите, идет обработка!");
    bytesCount = totalBytes;
    wordsCount = totalBytes;
    bytesBuff = new uint8_t [bytesCount];
    wordsBuff = new uint16_t[wordsCount];
    
    for ( int step = 0; step < 2; step++ )
    {
        iColumn = 0;
        for ( uint i = 0 ; i < TOTAL_FRAMES; i++)
        {
            imagePath = filename ;
            if ( i < 10 )
                imagePath += "000";
            else if ( i < 100 )
                imagePath += "00";
            else if ( i < 1000 )
                imagePath += "0";
            imagePath += tmp.setNum(i);
            imagePath += ".tif";
            //qDebug() << imagePath;
            tmp = "Подождите, идет обработка!\n";
            tmp.append( imagePath );
            emit sigProcessState(i/1.9, tmp );
            
            if(!tif.ReadTiff((char*)imagePath.toStdString().c_str()))
            {
                qDebug() << imagePath << " No image! ";
                continue;
            }
            //thread->usleep(1000000);
            //SetCentralLine(imagePath, 0);
            width = tif.Width();
            for ( int j = 0 ; j < tif.Width(); j++ )
            {
                iByte = j * totalFrames + iColumn;
                int n = 0;
                if ( step == 0)
                {
                    n = -7;
                }
                else if ( step == 1)
                {
                    n = 7;
                }
                wordsBuff[iByte] = 0;
                uint32_t tmp =  mPts[j].y;
                double dtmp = 0;
                dtmp = mPts[j].y - tmp;
                dtmp = dtmp * tif.Pixel(( tmp + 1 + n) * tif.Width() + j);
                //dtmp1 = dtmp1 * dtmp0;
                wordsBuff[iByte] = dtmp;
                dtmp = 1 - mPts[j].y + tmp;
                dtmp = dtmp * tif.Pixel((tmp + n) * tif.Width() + j);
                //dtmp1 = dtmp1 * dtmp0;
                wordsBuff[iByte] += dtmp;
                
                bytesBuff[iByte] = wordsBuff[iByte] / 256;
                //iByte++;
            }
            iColumn++;
        }
        height = width;
        width = totalFrames;
        imagePath = filename;
        if ( step == 0)
        {
            imagePath += "SunRed.tif";
            onSaveImage(imagePath, "tif");
        }
        else if ( step == 1 )
        {
            int32_t * doplerBuf = new int32_t [wordsCount];
            memset(doplerBuf, 0x0000, wordsCount);
            //std::valarray<int16_t>dopBuf(wordsCount);
            
            imagePath += "SunBlue.tif";
            onSaveImage(imagePath, "tif");
            //tiffimg->WriteTiff((char*) imagePath.toStdString().c_str());
            //tiffimg->WriteTiff((char*) imagePath.toStdString().c_str());
            imagePath = filename;
            imagePath += "SunRed.tif";
            tif.ReadTiff((char*)imagePath.toStdString().c_str());
            width = tif.Width();
            height = tif.Height();
            for ( uint j = 0 ; j < totalBytes; j++ )
            {
                wordsBuff[j] = tif.Pixel(j);
            }
            imagePath = filename;
            imagePath += "SunBlue.tif";
            tif.ReadTiff((char*)imagePath.toStdString().c_str());
            int32_t maxVal = 0;
            int32_t minVal = 0;
            for ( uint j = 0 ; j < totalBytes; j++ )
            {
                doplerBuf[j] = tif.Pixel(j);
                doplerBuf[j] = doplerBuf[j] - wordsBuff[j];
                
                //doplerBuf[j] += 0x7FFF;
                if ( doplerBuf[j] < minVal)
                    minVal = doplerBuf[j];
                if ( doplerBuf[j] > maxVal)
                    maxVal = doplerBuf[j];
                
                //doplerBuf[j] *= 3;
                //wordsBuff[j] = doplerBuf[j];
                //bytesBuff[j] = wordsBuff[j] / 256;
            }
            qDebug() << "minVal = " << minVal;
            qDebug() << "maxVal = " << maxVal;
            
            int32_t maxVal2 = 0;
            float k = maxVal + -minVal;
            k = 0xFFFF/k;
            for ( uint j = 0 ; j < totalBytes; j++ )
            {
                doplerBuf[j] += -minVal;
                wordsBuff[j] = doplerBuf[j] * k ;
                bytesBuff[j] = wordsBuff[j] / 256;
                if ( maxVal2 < wordsBuff[j])
                    maxVal2 = wordsBuff[j];
            }
            qDebug() << "maxVal2 = " << maxVal2;
            imagePath = filename;
            imagePath += "SunRed-Blue.tif";
            onSaveImage(imagePath, "tif");
            delete [] doplerBuf;
            //tiffimg->WriteTiff((char*) imagePath.toStdString().c_str());
            //imagePath = filename;
            //imagePath += "SunMid.tif";
            //tiffimg->ReadTiff((char*)imagePath.toStdString().c_str());
            //for ( int j = 0 ; j < totalBytes; j++ )
            //{
            //    wordsBuff[j] = tiffimg->u16buff [j];
            //}
        }
    }
    QImage img2(bytesBuff,
                width,
                height,
                width,
                QImage::Format_Grayscale8 );
    QPixmap pix2;
    pix2.convertFromImage(img2);
    emit sigImageReady(pix2);
    emit sigFinished();
    READY;

    return;
}
