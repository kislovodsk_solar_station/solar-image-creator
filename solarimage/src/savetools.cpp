#include "savetools.h"
#include "ui_savetools.h"

SaveTools::SaveTools(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SaveTools)
{
    ui->setupUi(this);
}

SaveTools::~SaveTools()
{
    delete ui;
}

void SaveTools::on_pushButton_clicked()
{
    if ( ui->rbTif->isChecked())
    {
        QString filename = QFileDialog::getSaveFileName(NULL,
                                                        "Save File",
                                                        NULL,
                                                        "Images (*.tif )");
        emit sigSaveImage(filename, ui->rbTif->text());
    }
    else if (ui->rbJpg->isChecked())
    {
        QString filename = QFileDialog::getSaveFileName(NULL,
                                                        "Save File",
                                                        NULL,
                                                        "Images (*.jpg )");
        emit sigSaveImage(filename, ui->rbJpg->text());
    }
    else if (ui->rbFits->isChecked())
    {
        QString filename = QFileDialog::getSaveFileName(NULL,
                                                        "Save File",
                                                        NULL,
                                                        "Images (*.fit )");
        emit sigSaveImage(filename, ui->rbFits->text());
    }
}
