#include "solarguidewdgt.h"
#include <ui_solarguidewdgt.h>

#include <QFileDialog>
#include <QDebug>
#include <QColor>
#include <QRgb>

//----------------------------------------------------------------------
//  Конструктор класса.
//  Создаются объекты, потоки и связи между объектами.
//
SolarGuideWdgt::SolarGuideWdgt(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SolarGuideWdgt)
{
    ui->setupUi(this);

    fileWatcher = new FileWatcher;
    thread = new QThread;
    fileWatcher->moveToThread(thread);
    pixmap = new QPixmap;
    image = new QImage;

    // Соединяем объекты
    connect(this, SIGNAL(openFile(QString )),
             fileWatcher, SLOT(Watch(QString)));
    connect(fileWatcher, SIGNAL(sigCenter(int,int)),
            this, SLOT(ShowImage(int,int)));
    connect(fileWatcher, SIGNAL(sigCenter2(int,int,int,int)),
            this, SLOT(ShowImage2(int,int,int,int)));
    ui->line->setParent(ui->viewLabel);
    ui->line2->setParent(ui->viewLabel);
    ui->line3->setParent(ui->viewLabel);
    ui->line4->setParent(ui->viewLabel);
    ui->line->hide();
    ui->line2->hide();
    ui->line3->hide();
    ui->line4->hide();
    thread->start();
}

//----------------------------------------------------------------------
//  Деструктор класса. Удаляются объекты, потоки.
//
SolarGuideWdgt::~SolarGuideWdgt()
{
    thread->quit();
    delete fileWatcher;
    delete pixmap;
    delete image;
    delete thread;
    delete ui;
}

//----------------------------------------------------------------------
//  Метод отображения изображения и линий найденного центра
//
void SolarGuideWdgt::ShowImage(int x, int y)
{
    ui->line->show();
    ui->line2->show();
    image->load(path, 0);
    *pixmap = QPixmap::fromImage(*image);

    ui->viewLabel->setScaledContents(true);
    ui->viewLabel->setPixmap(*pixmap);

    // установка линии по X
    int x1 = x*ui->viewLabel->width()/image->width();
    ui->line->move(x1, 0);
    ui->line->setFixedSize(1, ui->viewLabel->height());
    ui->line->show();
    qDebug () << "scaled x = " << x1;

    // установка линии по Y
    int y1 = y*ui->viewLabel->height()/image->height();
    ui->line2->move(0, y1);
    ui->line2->setFixedSize( ui->viewLabel->width(), 1);
    ui->line2->show();
    qDebug () << "scaled y = " << y1;

    QString str = "Координаты: ";
    QString str_x;
    QString str_y;
    str.append(str_x.setNum(x1));
    str.append(" ");
    str.append(str_y.setNum(y1));
    ui->textBrowser->setText(str);
}

//----------------------------------------------------------------------
//  Метод отображения изображения и линий найденного центра
//
void SolarGuideWdgt::ShowImage2(int x1, int x2, int y1, int y2)
{
    ui->line->show();
    ui->line2->show();
    ui->line3->show();
    ui->line4->show();
    image->load(path, 0);
    *pixmap = QPixmap::fromImage(*image);

    ui->viewLabel->setScaledContents(true);
    ui->viewLabel->setPixmap(*pixmap);

    // установка линии по X
    int x_1 = x1*ui->viewLabel->width()/image->width();
    ui->line->move(x_1, 0);
    ui->line->setFixedSize(1, ui->viewLabel->height());
    ui->line->show();
    qDebug () << "scaled x = " << x_1;

    // установка линии по X
    int x_2 = x2*ui->viewLabel->width()/image->width();
    ui->line3->move(x_2, 0);
    ui->line3->setFixedSize(1, ui->viewLabel->height());
    ui->line3->show();
    qDebug () << "scaled x = " << x_2;

    // установка линии по Y
    int y_1 = y1*ui->viewLabel->height()/image->height();
    ui->line2->move(0, y_1);
    ui->line2->setFixedSize( ui->viewLabel->width(), 1);
    ui->line2->show();
    qDebug () << "scaled y = " << y_1;

    // установка линии по Y
    int y_2 = y2*ui->viewLabel->height()/image->height();
    ui->line4->move(0, y_2);
    ui->line4->setFixedSize( ui->viewLabel->width(), 1);
    ui->line4->show();
    qDebug () << "scaled y = " << y_2;

    QString str = "Координаты: ";
    QString str_x;
    QString str_y;
    str.append(str_x.setNum(x_1));
    str.append(" ");
    str.append(str_y.setNum(y_1));
    ui->textBrowser->setText(str);
}

void SolarGuideWdgt::Open()
{
    path = QFileDialog::getOpenFileName(0,QFileDialog::tr("Откройте изображение"),
                                        "D:/NEW TELESCOP/Soft/Solar Patrol Telescope/bin",
                                        QFileDialog::tr("Image Files (*.tif *.jpg *.bmp) ;; "
                                                        "TIFF (*.tif) ;; JPEG (*.jpg) ;; Bitmap (*.bmp) "));
    if(path.isEmpty())
        return;
    // Сигнал, передающий путь к файлу в класс FileWatcher
    emit openFile(path);
    ShowImage(0,0);
}












