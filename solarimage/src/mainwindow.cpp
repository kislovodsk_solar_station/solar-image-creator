#include "mainwindow.h"
#include "ui_mainwindow.h"
#define SCAN_DIR "D:/Patrol spectraheliograph/Image/20160422_071240/"
#include <QMdiSubWindow>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //this->setCentralWidget(mdiArea);
    solarGuideWdgt = new SolarGuideWdgt();

    threadImageCreator = new QThread;
    solarImageCreator = new SolarImageCreator;
    solarImageCreator->moveToThread(threadImageCreator);

    ui->mdiArea->addSubWindow(solarGuideWdgt);
    ui->mdiArea->currentSubWindow()->hide();

    //solarGuideWdgt->hide();

    processBarDlg = new ProcessBarDlg(this);
    processBarDlg->setMaximum(1000);
    //-------------------------------------------------------------------------
    // Меню "Обработка"
    //
    connect(ui->actCreateImage,
            SIGNAL(triggered()),
            this,
            SLOT(ShowImageView()));

    connect(ui->actSetCentralLine,
            SIGNAL(triggered()),
            this,
            SLOT(ShowCentralLine()));

    connect(ui->actFilterImage,
            SIGNAL(triggered()),
            this,
            SLOT(FilterImage()));

    connect(ui->actFindCenter,
            SIGNAL(triggered()),
            solarGuideWdgt,
            SLOT(show()));

    connect(ui->actFindCenter,
            SIGNAL(triggered()),
            solarGuideWdgt,
            SLOT(Open()));

    connect(ui->actCreateDopler,
            SIGNAL(triggered()),
            this,
            SLOT(CreateDoplerImage()));


    //-------------------------------------------------------------------------
    // Соединяем сигналы с WainWindow с solarImageCreator
    //
    connect(this, SIGNAL(sigCreateImage()),
            solarImageCreator, SLOT(CreateImage()));
    connect(this, SIGNAL(sigCreateDoplerImage()),
            solarImageCreator, SLOT(CreateDoplerImage2()));
    connect(this, SIGNAL(sigSetCenralLine(QString, int)),
            solarImageCreator, SLOT(SetCentralLine(QString, int)));
    connect(this, SIGNAL(sigFilterImage()),
            solarImageCreator, SLOT(FilterImage()));
    connect(this, SIGNAL(sigCalcSpectroFocus()),
            solarImageCreator, SLOT(CalcSpectroFocus()));
    connect(solarImageCreator, SIGNAL(sigWarningMessage(QString)),
            this, SLOT(ShowWarningMessage(QString)));
    //-------------------------------------------------------------------------
    // Соединяем сигналы с ProgressBar'ом
    //

    connect(solarImageCreator, SIGNAL(sigStarted()),
            processBarDlg, SLOT(showDlg()));
    connect(solarImageCreator, SIGNAL(sigFinished()),
            processBarDlg, SLOT(hide()));
    connect(solarImageCreator, SIGNAL(sigProcessState(int, QString )),
            processBarDlg, SLOT(setValue(int, QString )));

    threadImageCreator->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

//-----------------------------------------------------------------------------
//  Вывод сообщения в StatusBar
//
void MainWindow::ShowStatusMessage( char * msg )
{
    ui->statusBar->showMessage(msg);
}

//-----------------------------------------------------------------------------
//  Вывод сообщения о предупреждении
//
void MainWindow::ShowWarningMessage( QString str)
{
    QMessageBox::warning( this, "Patrol Telescope App", str );
}

//-----------------------------------------------------------------------------
//  Сборка изображения из спектров
//
void MainWindow::ShowImageView()
{
    CreateNewSolarImageDlg(0);
    QString filepath = QFileDialog::getExistingDirectory(this,
                                                         tr("Open Directory"),
                                                         SCAN_DIR,
                                                         QFileDialog::ShowDirsOnly
                                                         | QFileDialog::DontResolveSymlinks);
    if (filepath.isNull())
        return;
    filepath.append("/");
    for ( int i = 0 ; i < filepath.length(); i++)
    {
        imagePath[i] = filepath.toStdString().c_str()[i];
    }
    qDebug () << imagePath;
    //QFileDialog::ShowDirsOnly
    if ( imagePath != "" )
    {
        solarImageCreator->filename = imagePath;
        //threadimageCreator->disconnect(solarImageCreator);
        //connect(threadimageCreator, SIGNAL(started()),
        //        solarImageCreator, SLOT(CreateImage()));
        //connect(solarImageCreator, SIGNAL(sigFinished()),
        //        threadimageCreator, SLOT(quit()));
        emit sigCreateImage();

    }
    else
        qDebug () << "No imagePath selected!!!";
}

//-----------------------------------------------------------------------------
//  Сборка доплерограммы из спектров
//
void MainWindow::CreateDoplerImage()
{
    CreateNewSolarImageDlg(0);
    QString filepath = QFileDialog::getExistingDirectory(this,
                                                         tr("Open Directory"),
                                                         SCAN_DIR,
                                                         QFileDialog::ShowDirsOnly
                                                         | QFileDialog::DontResolveSymlinks);
    if (filepath.isNull())
        return;
    filepath.append("/");
    for ( int i = 0 ; i < filepath.length(); i++)
    {
        imagePath[i] = filepath.toStdString().c_str()[i];
    }
    qDebug () << imagePath;

    //QFileDialog::ShowDirsOnly
    if ( imagePath != "" )
    {
        solarImageCreator->filename = imagePath;
        //threadimageCreator->disconnect(solarImageCreator);
        //connect(threadimageCreator, SIGNAL(started()),
        //        solarImageCreator, SLOT(CreateImage()));
        //connect(solarImageCreator, SIGNAL(sigFinished()),
        //        threadimageCreator, SLOT(quit()));
        emit sigCreateDoplerImage();

    }
    else
        qDebug () << "No imagePath selected!!!";
}

//-----------------------------------------------------------------------------
//  Определение центральной линии
//
void MainWindow::ShowCentralLine()
{
    CreateNewSolarImageDlg(0);
    QString filename = QFileDialog::getOpenFileName(this,
                                                    "Open File",
                                                    SCAN_DIR,
                                                    "Images (*.tif *.tiff)");
    if ( filename.isNull() )
        return;
    solarImageCreator->filename = filename;
    //    threadimageCreator->disconnect(solarImageCreator);
    //    connect(thread, SIGNAL(started()),
    //            solarImageCreator, SLOT(SetCentralLine()));
    //    connect(solarImageCreator, SIGNAL(sigFinished()),
    //            threadimageCreator, SLOT(quit()));
    //    threadimageCreator->start();
    emit sigSetCenralLine(filename, 0 );

}

//-----------------------------------------------------------------------------
//  Фильтрация изображения
//
void MainWindow::FilterImage()
{
    CreateNewSolarImageDlg(0);
    QString filename = QFileDialog::getOpenFileName(NULL,
                                                    "Open File",
                                                    SCAN_DIR,
                                                    "Images (*.tif *.tiff *.jpeg *.jpg)");
    //QString filename = SCAN_IMG;
    if ( filename.isNull() )
        return;
    solarImageCreator->filename = filename;
    //    threadimageCreator->disconnect(solarImageCreator);
    //    connect(threadimageCreator, SIGNAL(started()),
    //            solarImageCreator, SLOT(FilterImage()));
    //    connect(solarImageCreator, SIGNAL(sigFinished()),
    //            threadimageCreator, SLOT(quit()));
    //    threadimageCreator->start();
    emit sigFilterImage();
}

void MainWindow::CreateNewSolarImageDlg(uint16_t num)
{
    DeleteSolarImageDlg(0);
    solarImageCreatorDlg = new SolarImageCreatorDlg(ui->mdiArea);
    ui->mdiArea->addSubWindow(solarImageCreatorDlg);
    //-------------------------------------------------------------------------
    // Соединяем сигналы solarImageCreator с GUI solarImageCreatorDlg
    //
    connect(solarImageCreator, SIGNAL(sigShowSpectroFocus(QPixmap,
                                                          QPoint*,int,
                                                          QPoint*,int)),
            solarImageCreatorDlg, SLOT(ShowSpectroFocus(QPixmap,
                                                        QPoint*,int,
                                                        QPoint*,int)));
    connect(solarImageCreator, SIGNAL(sigImageReady(QPixmap)),
            solarImageCreatorDlg, SLOT(ShowImage(QPixmap)));
    connect(solarImageCreator, SIGNAL(sigCentralLineReady ( QPixmap,
                                                            QPoint*, int,
                                                            QPoint*, int ) ),
            solarImageCreatorDlg, SLOT(SetCentralLine ( QPixmap,
                                                        QPoint*, int,
                                                        QPoint*, int )));
    connect(solarImageCreator,
            SIGNAL(sigFilteredImageReady(QPixmap, QPoint*, int, QPoint*, int,
                                         QPoint*, int, QPoint*, int, QPoint*, int,
                                         QPoint*, int, QPoint*, int, QPoint*, int,
                                         QPoint*, int, QPoint*, int )),
            solarImageCreatorDlg,
            SLOT(ShowFilteredImage(QPixmap, QPoint*, int,
                                   QPoint*, int, QPoint*, int, QPoint*, int,
                                   QPoint*, int, QPoint*, int, QPoint*, int,
                                   QPoint*, int, QPoint*, int, QPoint*, int )));
    connect(solarImageCreatorDlg, SIGNAL(sigTranspChanged(int)),
            solarImageCreator, SLOT(TranspChanged(int)));
    connect(solarImageCreatorDlg, SIGNAL(sigPointsCountChanged(int)),
            solarImageCreator, SLOT(PointsCountChanged(int)));
    connect(solarImageCreatorDlg, SIGNAL(sigLimx1Changed(int)),
            solarImageCreator, SLOT(onLimx1Changed(int)));
    connect(solarImageCreatorDlg, SIGNAL(sigLimx2Changed(int)),
            solarImageCreator, SLOT(onLimx2Changed(int)));
    connect(solarImageCreatorDlg, SIGNAL(sigLimy1Changed(int)),
            solarImageCreator, SLOT(onLimy1Changed(int)));
    connect(solarImageCreatorDlg, SIGNAL(sigLimy2Changed(int)),
            solarImageCreator, SLOT(onLimy2Changed(int)));
    connect(solarImageCreatorDlg, SIGNAL(sigContrastChanged(int)),
            solarImageCreator, SLOT(onContrastChanged(int)));
    connect(solarImageCreatorDlg, SIGNAL(sigSaveImage(QString, QString)),
            solarImageCreator, SLOT(onSaveImage(QString, QString)));
    connect(solarImageCreatorDlg, SIGNAL(sigHorizontFiltChanged(bool)),
            solarImageCreator, SLOT(onHorizontFiltChanged(bool)));
    connect(solarImageCreatorDlg, SIGNAL(sigVerticalFiltChanged(bool)),
            solarImageCreator, SLOT(onVerticalFiltChanged(bool)));
    connect(solarImageCreator, SIGNAL(sigInitLimits( int, int, int, int)),
            solarImageCreatorDlg, SLOT(initLimits( int, int, int, int)));
}

void MainWindow::DeleteSolarImageDlg(uint16_t num)
{
    if (SolarImageCreatorDlg::number != 0 )
    {
        ui->mdiArea->removeSubWindow(solarImageCreatorDlg);
        solarImageCreatorDlg->disconnect();
        delete solarImageCreatorDlg;
    }
}
