#include "filtertools.h"
#include "ui_filtertools.h"
#include <QMouseEvent>
#include <QDebug>

FilterTools::FilterTools(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FilterTools)
{
    ui->setupUi(this);
    ui->checkBox->setChecked(false);
    ui->checkBox_2->setChecked(true);
}

FilterTools::~FilterTools()
{
    delete ui;
}

void FilterTools::setLimits ( int x1, int x2, int y1, int y2)
{
    blockSignals(true);
    this->ui->spinBox_3->setValue(x1);
    this->ui->spinBox_4->setValue(x2);
    this->ui->spinBox_5->setValue(y1);
    this->ui->spinBox_6->setValue(y2);
    this->ui->spinBox_3->setMinimum(x1);
    this->ui->spinBox_4->setMaximum(x2);
    this->ui->spinBox_5->setMinimum(y1);
    this->ui->spinBox_6->setMaximum(y2);
    blockSignals(false);
}

void FilterTools::on_spinBox_valueChanged(int arg1)
{
    emit sigInterpolPointsCountChanged(arg1);
}

void FilterTools::on_spinBox_2_valueChanged(int arg1)
{
    emit sigTranspChanged(arg1);
}

void FilterTools::on_spinBox_3_valueChanged(int arg1)
{
    emit sigLimx1Changed(arg1);
}

void FilterTools::on_spinBox_4_valueChanged(int arg1)
{
    emit sigLimx2Changed(arg1);
}

void FilterTools::on_spinBox_5_valueChanged(int arg1)
{
    emit sigLimy1Changed(arg1);
}

void FilterTools::on_spinBox_6_valueChanged(int arg1)
{
    emit sigLimy2Changed(arg1);

}

void FilterTools::on_checkBox_toggled(bool checked)
{
    emit sigHorizontFiltChanged(checked);
    ui->spinBox_5->setEnabled(checked);
    ui->spinBox_6->setEnabled(checked);
    ui->label_5->setEnabled(checked);
    ui->label_6->setEnabled(checked);
}

void FilterTools::on_checkBox_2_toggled(bool checked)
{
    emit sigVerticalFiltChanged(checked);
    ui->spinBox_3->setEnabled(checked);
    ui->spinBox_4->setEnabled(checked);
    ui->label_3->setEnabled(checked);
    ui->label_4->setEnabled(checked);
}

void FilterTools::on_spinBox_7_valueChanged(int arg1)
{
    emit sigContrastChanged(arg1);
}

void FilterTools::mouseMoveEvent(QMouseEvent *ev)
{
    this->move( this->x() + ev->x() - x1, this->y() + ev->y() - y1 );
}

void FilterTools::mousePressEvent(QMouseEvent *ev)
{
    x1 = ev->x();
    y1 = ev->y();
}
