#include "imagetiff.h"
#include <QDebug>

ImageTiff::ImageTiff(const char * mfilename)
{
    bytesCount = 0;
    ReadTiff(mfilename);
}

ImageTiff::ImageTiff()
{
    bytesCount = 0;
}

ImageTiff::~ImageTiff()
{

}

//-----------------------------------------------------------------------------
//
//
bool ImageTiff::ReadTiff( const char * mfilename)
{
    TIFF *tiff;
    uint16 photo, bps, spp, fillorder;
    int* tmp = new int;
    *tmp = 0;
    tsize_t stripSize;
    unsigned long imageOffset, result;
    int stripMax, stripCount;
    uchar tempbyte;
    PurgeBuffer();
    // Open the TIFF image
    if((tiff = TIFFOpen( mfilename, "r")) == NULL)
    {
        //qDebug () <<  "Could not open incoming image";
        return false;
    }

    // Check that it is of a type that we support
    if((TIFFGetField(tiff, TIFFTAG_BITSPERSAMPLE, &bps) == 0) || (bps != 1))
    {
        //qDebug () << "Either undefined or unsupported number of bits per sample";
        // return;
    }

    if((TIFFGetField(tiff, TIFFTAG_SAMPLESPERPIXEL, &spp) == 0) || (spp != 1))
    {
        //qDebug () << "Either undefined or unsupported number of samples per pixel";
        //return ;
    }

    // Read in the possibly multiple strips
    stripSize = TIFFStripSize (tiff);
    //qDebug () << "stripSize " << stripSize;
    stripMax = TIFFNumberOfStrips (tiff);
    //qDebug () << "stripMax " << stripMax;
    imageOffset = 0;

    bytesCount = TIFFNumberOfStrips (tiff) * stripSize;
    //qDebug () << "bytesCount " << bytesCount;
    rawBuff = new uint8_t [bytesCount];
    u16BuffSize = bytesCount / 2;
    u16buff = new uint16_t [bytesCount / 2];
    u8BuffSize = bytesCount / 2;
    u8buff = new uint8_t [bytesCount / 2];
    for ( stripCount = 0; stripCount < stripMax; stripCount++)
    {
        if((result = TIFFReadEncodedStrip (tiff,
                                           stripCount,
                                           rawBuff + imageOffset,
                                           stripSize)) == -1)
        {
            //qDebug () << "Read error on input strip number " <<  stripCount;
            //return;
        }

        imageOffset += result;
    }

    // Deal with photometric interpretations
    if(TIFFGetField(tiff, TIFFTAG_PHOTOMETRIC, &photo) == 0)
    {
        //qDebug () << "Image has an undefined photometric interpretation\n";
        //return ;
    }

    if( photo == PHOTOMETRIC_MINISWHITE)
    {
        // Flip bits
        //qDebug () << "Fixing the photometric interpretation";

        for( ulong i = 0; i < bytesCount; i++)
            rawBuff[i] = ~rawBuff[i];
    }

    // Deal with fillorder
    if ( TIFFGetField ( tiff, TIFFTAG_FILLORDER, &fillorder ) == 0)
    {
        //qDebug () << "Image has an undefined fillorder";
        //return;
    }
    fillorder = FILLORDER_MSB2LSB;
    if ( fillorder != FILLORDER_MSB2LSB )
    {
        // We need to swap bits -- ABCDEFGH becomes HGFEDCBA
        //qDebug () << "Fixing the fillorder";

        for( ulong i = 0; i < bytesCount; i++)
        {
            tempbyte = 0;
            if(rawBuff[i] & 128) tempbyte += 1;
            if(rawBuff[i] & 64) tempbyte += 2;
            if(rawBuff[i] & 32) tempbyte += 4;
            if(rawBuff[i] & 16) tempbyte += 8;
            if(rawBuff[i] & 8) tempbyte += 16;
            if(rawBuff[i] & 4) tempbyte += 32;
            if(rawBuff[i] & 2) tempbyte += 64;
            if(rawBuff[i] & 1) tempbyte += 128;
            rawBuff[i] = tempbyte;
        }
    }

    // Do whatever it is we do with the buffer -- we dump it in hex
    TIFFGetField ( tiff, TIFFTAG_IMAGEWIDTH, &width);
    //qDebug () << "Image width " << width;
    TIFFGetField ( tiff, TIFFTAG_IMAGELENGTH, &height);
    //qDebug () << "Image heigh " << height;
    TIFFGetField ( tiff, TIFFTAG_BITSPERSAMPLE, tmp);
    //qDebug () << "Image TIFFTAG_BITSPERSAMPLE " << *tmp;
    for( ulong i = 0, j = 0; i < bytesCount; i = i + 2)
    {
        u16buff[j] = (  rawBuff[i+1] << 8 ) | rawBuff[i];
        u8buff[j] = u16buff[j] / 256;
        j++;
    }
    // Close the file
    TIFFClose(tiff);

    //    ::FreeLibrary(hModule);
    return true;
}

//-----------------------------------------------------------------------------
//
//
void ImageTiff::WriteTiff( const char * mfilename,
                           uint16_t* buf,
                           int m_width,
                           int m_height,
                           ulong m_bytes)
{
    TIFF *tiff;
    outbuffer = new uint8_t [ m_bytes * 2 ];
    for ( ulong i = 0, j = 0; i < m_bytes; i++ )
    {
        outbuffer[j++] = buf[i] & 0xFF;
        outbuffer[j++] = (buf[i] >> 8) ;
    }
    // Open the TIFF file
    if((tiff = TIFFOpen( mfilename, "w")) == NULL)
    {
        printf("Could not open output.tif for writing\n");
        return ;
    }
    // We need to set some values for basic tags before we can add any data
    TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, m_width);
    TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, m_height);
    TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 1);
    TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, 1);
    TIFFSetField(tiff, TIFFTAG_ROWSPERSTRIP, m_height);

    TIFFSetField(tiff, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
    TIFFSetField(tiff, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
    TIFFSetField(tiff, TIFFTAG_FILLORDER, FILLORDER_MSB2LSB);
    TIFFSetField(tiff, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);

    TIFFSetField(tiff, TIFFTAG_XRESOLUTION, 1.0);
    TIFFSetField(tiff, TIFFTAG_YRESOLUTION, 1.0);
    TIFFSetField(tiff, TIFFTAG_RESOLUTIONUNIT, RESUNIT_NONE);
    TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 16);
    // Write the information to the file
    TIFFWriteEncodedStrip(tiff, 0, outbuffer, m_bytes * 2 );
    // Close the file
    TIFFClose(tiff);
    delete [] outbuffer;
}

//-----------------------------------------------------------------------------
//
//
void ImageTiff::PurgeBuffer()
{
    if ( bytesCount != 0 )
    {
        delete [] u16buff;
        delete [] rawBuff;
        delete [] u8buff;
        bytesCount = 0;
    }
}
