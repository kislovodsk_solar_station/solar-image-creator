#include "filewatcher.h"
#include <QTextStream>
#include <QDebug>
#include <QThread>

FileWatcher::FileWatcher(QObject *parent) : QObject(parent)
{
    width = 0;
    height = 0;
    maxIntensity_X = 0;
    maxIntensity_Y = 0;
    isNotFirst = false;
    qt_ntfs_permission_lookup++; // turn checking on
}

//----------------------------------------------------------------------
//  Метод отображения изображения и линий найденного центра
//
void FileWatcher::Watch(QString filename)
{
    fileInfo = new QFileInfo(filename);
    dTime = new QDateTime;
    *dTime = QDateTime::currentDateTimeUtc();
    QString str_Time;
    QString format = "yyyyMMdd_HHmmss";
    QString name = "SolarPositions_";
    name.append(dTime->toString(format));
    name.append(".txt");
    QFile file(name);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    //  Цикл слежения за файлом
    while(true)
    {
        this->thread()->msleep(10);
        fileInfo->refresh();
        *dTime = fileInfo->lastModified();
        if( str_Time != dTime->toString(format) && fileInfo->exists())
        {
            qDebug() << "----------------------------------------------";
            QFile imageFile(filename);
            while(!imageFile.open(QIODevice::ReadOnly))
            {
                qDebug() << "Ожидание файла...";
                this->thread()->msleep(20);
            }

            str_Time = dTime->toString(format);
            qDebug() << dTime->toString(format);
            FindCenter(filename);
            QTextStream out(&file);
               out << "Time: " << str_Time
                   << " Position: " << posX << " " << posY
                   << " Offset: " << offsetX << " " << offsetY
                   << "\n";
        }
    }
}

//-------------------------------------------------------------------------
//  Метод отображения изображения и линий найденного центра
//
void FileWatcher::FindCenter(QString filename)
{
    image = new QImage;
    image->load(filename);

    maxIntensity_X = 0;
    maxIntensity_Y = 0;
    posX = 0;
    posY = 0;
    width = image->width();
    height = image->height();
    intensity_X = new int[width];
    intensity_Y = new int[height];
    int x,y;

    // Обнуляем массивы интенсивностей в столбцах и строках
    for ( x=0; x < width; x++)
    {
        intensity_X[x] = 0;
    }

    for ( y=0; y < height; y++)
    {
        intensity_Y[y] = 0;
    }
    // Находим значения средних интенсивностей в столбцах (по у)
    // и среднее значение интенсивности по всем столбцам (по х)
    for ( x=0; x < width; x++)
    {
        for (y=0; y < height; y++)
        {
            intensity_X[x] = intensity_X[x] + qGray(image->pixel(x,y));//image->pixelIndex(x,y);
        }
        intensity_X[x] = intensity_X[x] / height;

        if ( maxIntensity_X < intensity_X[x])
        {
            posX = x;
            maxIntensity_X = intensity_X[x];
        }
        else
        if ( maxIntensity_X == intensity_X[x])
        {
            posX2 = x;
        }
    }
    qDebug () << "posX1 = " << posX;
    posX += (posX2 - posX) / 2;
    qDebug() << "Max of vertical intensity = " << maxIntensity_X;
    qDebug() << " posX = " << posX << " posX2 =" << posX2;

    // Находим значения средних интенсивностей в столбцах (по у)
    // и среднее значение интенсивности по всем столбцам (по х)
    for ( y=0; y < height; y++)
    {
        for (x=0; x < width; x++)
        {
            intensity_Y[y] = intensity_Y[y] + qGray(image->pixel(x,y));//image->pixelIndex(x,y);
        }
        intensity_Y[y] = intensity_Y[y] / width;

        if ( maxIntensity_Y < intensity_Y[y])
        {
            posY = y;
            maxIntensity_Y = intensity_Y[y];
        }
        else
        if ( maxIntensity_Y == intensity_Y[y])
        {
            posY2 = y;
            //maxIntensity_Y = intensity_Y[y];
        }
    }
    qDebug () << "posY1 = " << posY;
    posY += (posY2 - posY) / 2;
    qDebug() << "Max of horisontal intensity = " << maxIntensity_Y;
    qDebug() << " posY = " << posY << " posY2 =" << posY2;

    // Если первый заход - то задаем стартовые позиции
    if(isNotFirst == false)
    {
        startPosX = posX;
        startPosY = posY;
    }
    isNotFirst = true;

    // Расчет смещений диска относительно начальной позиции
    offsetX = posX - startPosX;
    offsetY = posY - startPosY;

    emit sigCenter(posX, posY);

    delete image;
    delete [] intensity_X;
    delete [] intensity_Y;
}

//-------------------------------------------------------------------------
//  Метод отображения изображения и линий найденного центра
//
void FileWatcher::FindCenter2(QString filename)
{
    image = new QImage;
    image->load(filename);

    maxIntensity_X = 0;
    maxIntensity_Y = 0;
    posX = 0;
    posY = 0;
    width = image->width();
    height = image->height();
    intensity_X = new int[width];
    intensity_Y = new int[height];
    // Обнуляем массивы интенсивностей в столбцах и строках
    memset(intensity_X, 0, width);
    memset(intensity_Y, 0, height);
    // Находим значения средних интенсивностей в столбцах (по у)
    // и среднее значение интенсивности по всем столбцам (по х)
    for ( int x = 0; x < width; x++)
    {
        for (int y = 0; y < height; y++)
        {
            intensity_X[x] = intensity_X[x] + qGray(image->pixel(x,y));//image->pixelIndex(x,y);
        }
        intensity_X[x] = intensity_X[x] / height;
        if ( intensity_X[x] > 0x10 )
        {
            posX = x;
            //qDebug () << "x = " << x;
        }
        else if ( intensity_X[x] < 0x10 && posX != 0)
        {
            posX2 = x;
            break;
            //qDebug () << "x = " << x;

        }


    }



    emit sigCenter2(posX, posX2, posY, posY2);

    delete image;
    delete [] intensity_X;
    delete [] intensity_Y;
}
