#-------------------------------------------------
#
# Project created by QtCreator 2016-04-27T08:40:00
#
#-------------------------------------------------

QT       += core gui
CONFIG += c++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = solarimage
TEMPLATE = app

MOC_DIR = tmp/moc
OBJECTS_DIR = tmp/obj
UI_DIR = tmp/ui
RCC_DIR = tmp/rcc
DESTDIR = bin

INCLUDEPATH += include

LIBS += -L"./lib/" -lImageLib
LIBS += -L"./lib/" -llibtiff
LIBS += -L"./lib/" -lcfitsio

SOURCES += src/main.cpp\
    src/mainwindow.cpp \
    src/filtertools.cpp \
    src/fitsfile.cpp \
    src/imagetiff.cpp \
    src/processbardlg.cpp \
    src/savetools.cpp \
    src/solarimagecreator.cpp \
    src/solarimagecreatordlg.cpp \
    src/spline.cpp \
    src/filewatcher.cpp \
    src/solarguidewdgt.cpp

HEADERS  += include/tiffio.h \
    include/tiff.h \
    include/tiffconf.h \
    include/tiffvers.h \
    include/ImageLib.h \
    src/mainwindow.h \
    src/filtertools.h \
    src/fitsfile.h \
    src/imagetiff.h \
    src/processbardlg.h \
    src/savetools.h \
    src/solarimagecreator.h \
    src/solarimagecreatordlg.h \
    src/spline.h \
    src/filewatcher.h \
    src/solarguidewdgt.h


FORMS    += ui/mainwindow.ui \
    ui/filtertools.ui \
    ui/processbardlg.ui \
    ui/savetools.ui \
    ui/solarimagecreatordlg.ui \
    ui/solarguidewdgt.ui
