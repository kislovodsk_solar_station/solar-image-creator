/* $Id: tiffio.h,v 1.50 2006/03/21 16:37:51 dron Exp $ */

/*
 * Copyright (c) 1988-1997 Sam Leffler
 * Copyright (c) 1991-1997 Silicon Graphics, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and 
 * its documentation for any purpose is hereby granted without fee, provided
 * that (i) the above copyright notices and this permission notice appear in
 * all copies of the software and related documentation, and (ii) the names of
 * Sam Leffler and Silicon Graphics may not be used in any advertising or
 * publicity relating to the software without the specific, prior written
 * permission of Sam Leffler and Silicon Graphics.
 * 
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 * IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 * ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 * OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF
 * LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

#ifndef _TIFFIO_
#define	_TIFFIO_

#ifndef __GNUC__
# define __DLL_IMPORT__  __declspec(dllimport)
# define __DLL_EXPORT__  __declspec(dllexport)
#else
# define __DLL_IMPORT__  __attribute__((dllimport)) extern
# define __DLL_EXPORT__  __attribute__((dllexport)) extern
#endif 

#if (defined __WIN32__) || (defined _WIN32)
# ifdef BUILD_LIBTIFF_DLL
#  define LIBTIFF_DLL_IMPEXP     __DLL_EXPORT__
# elif defined(LIBTIFF_STATIC)
#  define LIBTIFF_DLL_IMPEXP      
# elif defined (USE_LIBTIFF_DLL)
#  define LIBTIFF_DLL_IMPEXP     __DLL_IMPORT__
# elif defined (USE_LIBTIFF_STATIC)
#  define LIBTIFF_DLL_IMPEXP      
# else /* assume USE_LIBTIFF_DLL */
#  define LIBTIFF_DLL_IMPEXP     __DLL_IMPORT__
# endif
#else /* __WIN32__ */
# define LIBTIFF_DLL_IMPEXP  
#endif

/*
 * TIFF I/O Library Definitions.
 */
#include "tiff.h"
#include "tiffvers.h"

/*
 * TIFF is defined as an incomplete type to hide the
 * library's internal data structures from clients.
 */
typedef	struct tiff TIFF;

/*
 * The following typedefs define the intrinsic size of
 * data types used in the *exported* interfaces.  These
 * definitions depend on the proper definition of types
 * in tiff.h.  Note also that the varargs interface used
 * to pass tag types and values uses the types defined in
 * tiff.h directly.
 *
 * NB: ttag_t is unsigned int and not unsigned short because
 *     ANSI C requires that the type before the ellipsis be a
 *     promoted type (i.e. one of int, unsigned int, pointer,
 *     or double) and because we defined pseudo-tags that are
 *     outside the range of legal Aldus-assigned tags.
 * NB: tsize_t is int32 and not uint32 because some functions
 *     return -1.
 * NB: toff_t is not off_t for many reasons; TIFFs max out at
 *     32-bit file offsets being the most important, and to ensure
 *     that it is unsigned, rather than signed.
 */
typedef	uint32 ttag_t;		/* directory tag */
typedef	uint16 tdir_t;		/* directory index */
typedef	uint16 tsample_t;	/* sample number */
typedef	uint32 tstrip_t;	/* strip number */
typedef uint32 ttile_t;		/* tile number */
typedef	int32 tsize_t;		/* i/o size in bytes */
typedef	void* tdata_t;		/* image data ref */
typedef	uint32 toff_t;		/* file offset */

#if !defined(__WIN32__) && (defined(_WIN32) || defined(WIN32))
#define __WIN32__
#endif

/*
 * On windows you should define USE_WIN32_FILEIO if you are using tif_win32.c
 * or AVOID_WIN32_FILEIO if you are using something else (like tif_unix.c).
 *
 * By default tif_unix.c is assumed.
 */

#if defined(_WINDOWS) || defined(__WIN32__) || defined(_Windows)
#  define BINMODE "b"
#  if !defined(__CYGWIN) && !defined(AVOID_WIN32_FILEIO) && !defined(USE_WIN32_FILEIO)
#    define AVOID_WIN32_FILEIO
#  endif
#  include <fcntl.h>
#  include <io.h>
#  ifdef SET_BINARY
#    undef SET_BINARY
#  endif /* SET_BINARY */
#  define SET_BINARY(f) do {if (!_isatty(f)) _setmode(f,_O_BINARY);} while (0)
#else /* Windows */
#  define BINMODE
#  define SET_BINARY(f) (void)0
#endif /* Windows */

#if defined(USE_WIN32_FILEIO)
# define VC_EXTRALEAN
# include <windows.h>
# ifdef __WIN32__
DECLARE_HANDLE(thandle_t);	/* Win32 file handle */
# else
typedef	HFILE thandle_t;	/* client data handle */
# endif /* __WIN32__ */
#else
typedef	void* thandle_t;	/* client data handle */
#endif /* USE_WIN32_FILEIO */

#ifndef NULL
# define NULL	(void *)0
#endif

/*
 * Flags to pass to TIFFPrintDirectory to control
 * printing of data structures that are potentially
 * very large.   Bit-or these flags to enable printing
 * multiple items.
 */
#define	TIFFPRINT_NONE		0x0		/* no extra info */
#define	TIFFPRINT_STRIPS	0x1		/* strips/tiles info */
#define	TIFFPRINT_CURVES	0x2		/* color/gray response curves */
#define	TIFFPRINT_COLORMAP	0x4		/* colormap */
#define	TIFFPRINT_JPEGQTABLES	0x100		/* JPEG Q matrices */
#define	TIFFPRINT_JPEGACTABLES	0x200		/* JPEG AC tables */
#define	TIFFPRINT_JPEGDCTABLES	0x200		/* JPEG DC tables */

/* 
 * Colour conversion stuff
 */

/* reference white */
#define D65_X0 (95.0470F)
#define D65_Y0 (100.0F)
#define D65_Z0 (108.8827F)

#define D50_X0 (96.4250F)
#define D50_Y0 (100.0F)
#define D50_Z0 (82.4680F)

/* Structure for holding information about a display device. */

typedef	unsigned char TIFFRGBValue;		/* 8-bit samples */

typedef struct {
	float d_mat[3][3]; 		/* XYZ -> luminance matrix */
	float d_YCR;			/* Light o/p for reference white */
	float d_YCG;
	float d_YCB;
	uint32 d_Vrwr;			/* Pixel values for ref. white */
	uint32 d_Vrwg;
	uint32 d_Vrwb;
	float d_Y0R;			/* Residual light for black pixel */
	float d_Y0G;
	float d_Y0B;
	float d_gammaR;			/* Gamma values for the three guns */
	float d_gammaG;
	float d_gammaB;
} TIFFDisplay;

typedef struct {				/* YCbCr->RGB support */
	TIFFRGBValue* clamptab;			/* range clamping table */
	int*	Cr_r_tab;
	int*	Cb_b_tab;
	int32*	Cr_g_tab;
	int32*	Cb_g_tab;
        int32*  Y_tab;
} TIFFYCbCrToRGB;

typedef struct {				/* CIE Lab 1976->RGB support */
	int	range;				/* Size of conversion table */
#define CIELABTORGB_TABLE_RANGE 1500
	float	rstep, gstep, bstep;
	float	X0, Y0, Z0;			/* Reference white point */
	TIFFDisplay display;
	float	Yr2r[CIELABTORGB_TABLE_RANGE + 1];  /* Conversion of Yr to r */
	float	Yg2g[CIELABTORGB_TABLE_RANGE + 1];  /* Conversion of Yg to g */
	float	Yb2b[CIELABTORGB_TABLE_RANGE + 1];  /* Conversion of Yb to b */
} TIFFCIELabToRGB;

/*
 * RGBA-style image support.
 */
typedef struct _TIFFRGBAImage TIFFRGBAImage;
/*
 * The image reading and conversion routines invoke
 * ``put routines'' to copy/image/whatever tiles of
 * raw image data.  A default set of routines are 
 * provided to convert/copy raw image data to 8-bit
 * packed ABGR format rasters.  Applications can supply
 * alternate routines that unpack the data into a
 * different format or, for example, unpack the data
 * and draw the unpacked raster on the display.
 */
typedef void (*tileContigRoutine)
    (TIFFRGBAImage*, uint32*, uint32, uint32, uint32, uint32, int32, int32,
	unsigned char*);
typedef void (*tileSeparateRoutine)
    (TIFFRGBAImage*, uint32*, uint32, uint32, uint32, uint32, int32, int32,
	unsigned char*, unsigned char*, unsigned char*, unsigned char*);
/*
 * RGBA-reader state.
 */
struct _TIFFRGBAImage {
	TIFF*	tif;				/* image handle */
	int	stoponerr;			/* stop on read error */
	int	isContig;			/* data is packed/separate */
	int	alpha;				/* type of alpha data present */
	uint32	width;				/* image width */
	uint32	height;				/* image height */
	uint16	bitspersample;			/* image bits/sample */
	uint16	samplesperpixel;		/* image samples/pixel */
	uint16	orientation;			/* image orientation */
	uint16	req_orientation;		/* requested orientation */
	uint16	photometric;			/* image photometric interp */
	uint16*	redcmap;			/* colormap pallete */
	uint16*	greencmap;
	uint16*	bluecmap;
						/* get image data routine */
	int	(*get)(TIFFRGBAImage*, uint32*, uint32, uint32);
	union {
	    void (*any)(TIFFRGBAImage*);
	    tileContigRoutine	contig;
	    tileSeparateRoutine	separate;
	} put;					/* put decoded strip/tile */
	TIFFRGBValue* Map;			/* sample mapping array */
	uint32** BWmap;				/* black&white map */
	uint32** PALmap;			/* palette image map */
	TIFFYCbCrToRGB* ycbcr;			/* YCbCr conversion state */
        TIFFCIELabToRGB* cielab;		/* CIE L*a*b conversion state */

        int	row_offset;
        int     col_offset;
};

/*
 * Macros for extracting components from the
 * packed ABGR form returned by TIFFReadRGBAImage.
 */
#define	TIFFGetR(abgr)	((abgr) & 0xff)
#define	TIFFGetG(abgr)	(((abgr) >> 8) & 0xff)
#define	TIFFGetB(abgr)	(((abgr) >> 16) & 0xff)
#define	TIFFGetA(abgr)	(((abgr) >> 24) & 0xff)

/*
 * A CODEC is a software package that implements decoding,
 * encoding, or decoding+encoding of a compression algorithm.
 * The library provides a collection of builtin codecs.
 * More codecs may be registered through calls to the library
 * and/or the builtin implementations may be overridden.
 */
typedef	int (*TIFFInitMethod)(TIFF*, int);
typedef struct {
	char*		name;
	uint16		scheme;
	TIFFInitMethod	init;
} TIFFCodec;

#include <stdio.h>
#include <stdarg.h>

/* share internal LogLuv conversion routines? */
#ifndef LOGLUV_PUBLIC
#define LOGLUV_PUBLIC		1
#endif

#if defined(c_plusplus) || defined(__cplusplus)
extern "C" {
#endif
typedef	void (*TIFFErrorHandler)(const char*, const char*, va_list);
typedef	void (*TIFFErrorHandlerExt)(thandle_t, const char*, const char*, va_list);
typedef	tsize_t (*TIFFReadWriteProc)(thandle_t, tdata_t, tsize_t);
typedef	toff_t (*TIFFSeekProc)(thandle_t, toff_t, int);
typedef	int (*TIFFCloseProc)(thandle_t);
typedef	toff_t (*TIFFSizeProc)(thandle_t);
typedef	int (*TIFFMapFileProc)(thandle_t, tdata_t*, toff_t*);
typedef	void (*TIFFUnmapFileProc)(thandle_t, tdata_t, toff_t);
typedef	void (*TIFFExtendProc)(TIFF*); 

LIBTIFF_DLL_IMPEXP	const char* TIFFGetVersion(void);

LIBTIFF_DLL_IMPEXP	const TIFFCodec* TIFFFindCODEC(uint16);
LIBTIFF_DLL_IMPEXP	TIFFCodec* TIFFRegisterCODEC(uint16, const char*, TIFFInitMethod);
LIBTIFF_DLL_IMPEXP	void TIFFUnRegisterCODEC(TIFFCodec*);
LIBTIFF_DLL_IMPEXP  int TIFFIsCODECConfigured(uint16);
LIBTIFF_DLL_IMPEXP	TIFFCodec* TIFFGetConfiguredCODECs(void);

/*
 * Auxiliary functions.
 */

LIBTIFF_DLL_IMPEXP	tdata_t _TIFFmalloc(tsize_t);
LIBTIFF_DLL_IMPEXP	tdata_t _TIFFrealloc(tdata_t, tsize_t);
LIBTIFF_DLL_IMPEXP	void _TIFFmemset(tdata_t, int, tsize_t);
LIBTIFF_DLL_IMPEXP	void _TIFFmemcpy(tdata_t, const tdata_t, tsize_t);
LIBTIFF_DLL_IMPEXP	int _TIFFmemcmp(const tdata_t, const tdata_t, tsize_t);
LIBTIFF_DLL_IMPEXP	void _TIFFfree(tdata_t);

/*
** Stuff, related to tag handling and creating custom tags.
*/
LIBTIFF_DLL_IMPEXP  int  TIFFGetTagListCount( TIFF * );
LIBTIFF_DLL_IMPEXP  ttag_t TIFFGetTagListEntry( TIFF *, int tag_index );
    
#define	TIFF_ANY	TIFF_NOTYPE	/* for field descriptor searching */
#define	TIFF_VARIABLE	-1		/* marker for variable length tags */
#define	TIFF_SPP	-2		/* marker for SamplesPerPixel tags */
#define	TIFF_VARIABLE2	-3		/* marker for uint32 var-length tags */

#define FIELD_CUSTOM    65    

typedef	struct {
	ttag_t	field_tag;		/* field's tag */
	short	field_readcount;	/* read count/TIFF_VARIABLE/TIFF_SPP */
	short	field_writecount;	/* write count/TIFF_VARIABLE */
	TIFFDataType field_type;	/* type of associated data */
        unsigned short field_bit;	/* bit in fieldsset bit vector */
	unsigned char field_oktochange;	/* if true, can change while writing */
	unsigned char field_passcount;	/* if true, pass dir count on set */
	char	*field_name;		/* ASCII name */
} TIFFFieldInfo;

typedef struct _TIFFTagValue {
    const TIFFFieldInfo  *info;
    int             count;
    void           *value;
} TIFFTagValue;

LIBTIFF_DLL_IMPEXP	void TIFFMergeFieldInfo(TIFF*, const TIFFFieldInfo[], int);
LIBTIFF_DLL_IMPEXP	const TIFFFieldInfo* TIFFFindFieldInfo(TIFF*, ttag_t, TIFFDataType);
LIBTIFF_DLL_IMPEXP  const TIFFFieldInfo* TIFFFindFieldInfoByName(TIFF* , const char *,
						     TIFFDataType);
LIBTIFF_DLL_IMPEXP	const TIFFFieldInfo* TIFFFieldWithTag(TIFF*, ttag_t);
LIBTIFF_DLL_IMPEXP	const TIFFFieldInfo* TIFFFieldWithName(TIFF*, const char *);

typedef	int (*TIFFVSetMethod)(TIFF*, ttag_t, va_list);
typedef	int (*TIFFVGetMethod)(TIFF*, ttag_t, va_list);
typedef	void (*TIFFPrintMethod)(TIFF*, FILE*, long);
    
typedef struct {
    TIFFVSetMethod	vsetfield;	/* tag set routine */
    TIFFVGetMethod	vgetfield;	/* tag get routine */
    TIFFPrintMethod	printdir;	/* directory print routine */
} TIFFTagMethods;
        
typedef  TIFFTagMethods *(*TIFFAccessTagMethods_t)( TIFF * );
typedef  void *(*TIFFGetClientInfo_t)( TIFF *, const char * );
typedef  void (*TIFFSetClientInfo_t)( TIFF *, void *, const char * );

typedef	void (*TIFFCleanup_t)(TIFF*);
typedef void (*TIFFClose_t)(TIFF*);
typedef	int (*TIFFFlush_t)(TIFF*);
typedef	int (*TIFFFlushData_t)(TIFF*);
typedef	int (*TIFFGetField_t)(TIFF*, ttag_t, ...);
typedef	int (*TIFFVGetField_t)(TIFF*, ttag_t, va_list);
typedef	int (*TIFFGetFieldDefaulted_t)(TIFF*, ttag_t, ...);
typedef	int (*TIFFVGetFieldDefaulted_t)(TIFF*, ttag_t, va_list);
typedef	int (*TIFFReadDirectory_t)(TIFF*);
typedef	int (*TIFFReadCustomDirectory_t)(TIFF*, toff_t, const TIFFFieldInfo[],
				    size_t);
typedef	int (*TIFFReadEXIFDirectory_t)(TIFF*, toff_t);
typedef	tsize_t (*TIFFScanlineSize_t)(TIFF*);
typedef	tsize_t (*TIFFRasterScanlineSize_t)(TIFF*);
typedef	tsize_t (*TIFFStripSize_t)(TIFF*);
typedef	tsize_t (*TIFFRawStripSize_t)(TIFF*, tstrip_t);
typedef	tsize_t (*TIFFVStripSize_t)(TIFF*, uint32);
typedef	tsize_t (*TIFFTileRowSize_t)(TIFF*);
typedef	tsize_t (*TIFFTileSize_t)(TIFF*);
typedef	tsize_t (*TIFFVTileSize_t)(TIFF*, uint32);
typedef	uint32 (*TIFFDefaultStripSize_t)(TIFF*, uint32);
typedef	void (*TIFFDefaultTileSize_t)(TIFF*, uint32*, uint32*);
typedef	int (*TIFFFileno_t)(TIFF*);
typedef  int (*TIFFSetFileno_t)(TIFF*, int);
typedef  thandle_t (*TIFFClientdata_t)(TIFF*);
typedef  thandle_t (*TIFFSetClientdata_t)(TIFF*, thandle_t);
typedef	int (*TIFFGetMode_t)(TIFF*);
typedef	int (*TIFFSetMode_t)(TIFF*, int);
typedef	int (*TIFFIsTiled_t)(TIFF*);
typedef	int (*TIFFIsByteSwapped_t)(TIFF*);
typedef	int (*TIFFIsUpSampled_t)(TIFF*);
typedef	int (*TIFFIsMSB2LSB_t)(TIFF*);
typedef	int (*TIFFIsBigEndian_t)(TIFF*);
typedef	TIFFReadWriteProc (*TIFFGetReadProc_t)(TIFF*);
typedef	TIFFReadWriteProc (*TIFFGetWriteProc_t)(TIFF*);
typedef	TIFFSeekProc (*TIFFGetSeekProc_t)(TIFF*);
typedef	TIFFCloseProc (*TIFFGetCloseProc_t)(TIFF*);
typedef	TIFFSizeProc (*TIFFGetSizeProc_t)(TIFF*);
typedef	TIFFMapFileProc (*TIFFGetMapFileProc_t)(TIFF*);
typedef	TIFFUnmapFileProc (*TIFFGetUnmapFileProc_t)(TIFF*);
typedef	uint32 (*TIFFCurrentRow_t)(TIFF*);
typedef	tdir_t (*TIFFCurrentDirectory_t)(TIFF*);
typedef	tdir_t (*TIFFNumberOfDirectories_t)(TIFF*);
typedef	uint32 (*TIFFCurrentDirOffset_t)(TIFF*);
typedef	tstrip_t (*TIFFCurrentStrip_t)(TIFF*);
typedef	ttile_t (*TIFFCurrentTile_t)(TIFF*);
typedef	int (*TIFFReadBufferSetup_t)(TIFF*, tdata_t, tsize_t);
typedef	int (*TIFFWriteBufferSetup_t)(TIFF*, tdata_t, tsize_t);
typedef	int (*TIFFSetupStrips_t)(TIFF *);
typedef  int (*TIFFWriteCheck_t)(TIFF*, int, const char *);
typedef	void (*TIFFFreeDirectory_t)(TIFF*);
typedef  int (*TIFFCreateDirectory_t)(TIFF*);
typedef	int (*TIFFLastDirectory_t)(TIFF*);
typedef	int (*TIFFSetDirectory_t)(TIFF*, tdir_t);
typedef	int (*TIFFSetSubDirectory_t)(TIFF*, uint32);
typedef	int (*TIFFUnlinkDirectory_t)(TIFF*, tdir_t);
typedef int (*TIFFSetField_t)(TIFF*, ttag_t, ...);
typedef	int (*TIFFVSetField_t)(TIFF*, ttag_t, va_list);
typedef	int (*TIFFWriteDirectory_t)(TIFF *);
typedef	int (*TIFFCheckpointDirectory_t)(TIFF *);
typedef	int (*TIFFRewriteDirectory_t)(TIFF *);
typedef	int (*TIFFReassignTagToIgnore_t)(enum TIFFIgnoreSense, int);

#if defined(c_plusplus) || defined(__cplusplus)
typedef	void (*TIFFPrintDirectory_t)(TIFF*, FILE*, long );
typedef	int (*TIFFReadScanline_t)(TIFF*, tdata_t, uint32, tsample_t);
typedef	int (*TIFFWriteScanline_t)(TIFF*, tdata_t, uint32, tsample_t );
typedef	int (*TIFFReadRGBAImage_t)(TIFF*, uint32, uint32, uint32*, int);
typedef	int (*TIFFReadRGBAImageOriented_t)(TIFF*, uint32, uint32, uint32*,
                      int , int );
//typedef	int (*TIFFReadRGBAImageOriented_t)(TIFF*, uint32, uint32, uint32*,
//                      int = ORIENTATION_BOTLEFT, int );
#else
typedef	void (*TIFFPrintDirectory_t)(TIFF*, FILE*, long);
typedef	int (*TIFFReadScanline_t)(TIFF*, tdata_t, uint32, tsample_t);
typedef	int (*TIFFWriteScanline_t)(TIFF*, tdata_t, uint32, tsample_t);
typedef	int (*TIFFReadRGBAImage_t)(TIFF*, uint32, uint32, uint32*, int);
typedef	int (*TIFFReadRGBAImageOriented_t)(TIFF*, uint32, uint32, uint32*, int, int);
#endif

typedef	int (*TIFFReadRGBAStrip_t)(TIFF*, tstrip_t, uint32 * );
typedef	int (*TIFFReadRGBATile_t)(TIFF*, uint32, uint32, uint32 * );
typedef	int (*TIFFRGBAImageOK_t)(TIFF*, char [1024]);
typedef	int (*TIFFRGBAImageBegin_t)(TIFFRGBAImage*, TIFF*, int, char [1024]);
typedef	int (*TIFFRGBAImageGet_t)(TIFFRGBAImage*, uint32*, uint32, uint32);
typedef	void (*TIFFRGBAImageEnd_t)(TIFFRGBAImage*);
typedef TIFF* (*TIFFOpen_t)(const char*, const char*);
# ifdef __WIN32__
typedef	TIFF* (*TIFFOpenW_t)(const wchar_t*, const char*);
# endif /* __WIN32__ */
typedef	TIFF* (*TIFFFdOpen_t)(int, const char*, const char*);
typedef	TIFF* (*TIFFClientOpen_t)(const char*, const char*,
	    thandle_t,
	    TIFFReadWriteProc, TIFFReadWriteProc,
	    TIFFSeekProc, TIFFCloseProc,
	    TIFFSizeProc,
	    TIFFMapFileProc, TIFFUnmapFileProc);
typedef	const char* (*TIFFFileName_t)(TIFF*);
typedef	const char* (*TIFFSetFileName_t)(TIFF*, const char *);
typedef	void (*TIFFError_t)(const char*, const char*, ...);
typedef	void (*TIFFErrorExt_t)(thandle_t, const char*, const char*, ...);
typedef	void (*TIFFWarning_t)(const char*, const char*, ...);
typedef	void (*TIFFWarningExt_t)(thandle_t, const char*, const char*, ...);
typedef	TIFFErrorHandler (*TIFFSetErrorHandler_t)(TIFFErrorHandler);
typedef	TIFFErrorHandlerExt (*TIFFSetErrorHandlerExt_t)(TIFFErrorHandlerExt);
typedef	TIFFErrorHandler (*TIFFSetWarningHandler_t)(TIFFErrorHandler);
typedef	TIFFErrorHandlerExt (*TIFFSetWarningHandlerExt_t)(TIFFErrorHandlerExt);
typedef	TIFFExtendProc (*TIFFSetTagExtender_t)(TIFFExtendProc);
typedef	ttile_t (*TIFFComputeTile_t)(TIFF*, uint32, uint32, uint32, tsample_t);
typedef	int (*TIFFCheckTile_t)(TIFF*, uint32, uint32, uint32, tsample_t);
typedef	ttile_t (*TIFFNumberOfTiles_t)(TIFF*);
typedef	tsize_t (*TIFFReadTile_t)(TIFF*,
	    tdata_t, uint32, uint32, uint32, tsample_t);
typedef	tsize_t (*TIFFWriteTile_t)(TIFF*,
	    tdata_t, uint32, uint32, uint32, tsample_t);
typedef	tstrip_t (*TIFFComputeStrip_t)(TIFF*, uint32, tsample_t);
typedef	tstrip_t (*TIFFNumberOfStrips_t)(TIFF*);
typedef	tsize_t (*TIFFReadEncodedStrip_t)(TIFF*, tstrip_t, tdata_t, tsize_t);
typedef	tsize_t (*TIFFReadRawStrip_t)(TIFF*, tstrip_t, tdata_t, tsize_t);
typedef	tsize_t (*TIFFReadEncodedTile_t)(TIFF*, ttile_t, tdata_t, tsize_t);
typedef	tsize_t (*TIFFReadRawTile_t)(TIFF*, ttile_t, tdata_t, tsize_t);
typedef tsize_t (*TIFFWriteEncodedStrip_t)(TIFF*, tstrip_t, tdata_t, tsize_t);
typedef	tsize_t (*TIFFWriteRawStrip_t)(TIFF*, tstrip_t, tdata_t, tsize_t);
typedef	tsize_t (*TIFFWriteEncodedTile_t)(TIFF*, ttile_t, tdata_t, tsize_t);
typedef	tsize_t (*TIFFWriteRawTile_t)(TIFF*, ttile_t, tdata_t, tsize_t);
typedef	int (*TIFFDataWidth_t)(TIFFDataType);    /* table of tag datatype widths */
typedef	void (*TIFFSetWriteOffset_t)(TIFF*, toff_t);
typedef	void (*TIFFSwabShort_t)(uint16*);
typedef	void (*TIFFSwabLong_t)(uint32*);
typedef	void (*TIFFSwabDouble_t)(double*);
typedef	void (*TIFFSwabArrayOfShort_t)(uint16*, unsigned long);
typedef	void (*TIFFSwabArrayOfTriples_t)(uint8*, unsigned long);
typedef	void (*TIFFSwabArrayOfLong_t)(uint32*, unsigned long);
typedef	void (*TIFFSwabArrayOfDouble_t)(double*, unsigned long);
typedef	void (*TIFFReverseBits_t)(unsigned char *, unsigned long);
typedef	const unsigned char* (*TIFFGetBitRevTable_t)(int);

#ifdef LOGLUV_PUBLIC
#define U_NEU		0.210526316
#define V_NEU		0.473684211
#define UVSCALE		410.
typedef	double (*LogL16toY_t)(int);
typedef	double (*LogL10toY_t)(int);
typedef	void (*XYZtoRGB24_t)(float*, uint8*);
typedef	int (*uv_decode_t)(double*, double*, int);
typedef	void (*LogLuv24toXYZ_t)(uint32, float*);
typedef	void (*LogLuv32toXYZ_t)(uint32, float*);
#if defined(c_plusplus) || defined(__cplusplus)
typedef	int (*LogL16fromY_t)(double, int );// = SGILOGENCODE_NODITHER
typedef	int (*LogL10fromY_t)(double, int );// = SGILOGENCODE_NODITHER
typedef	int (*uv_encode_t)(double, double, int );// = SGILOGENCODE_NODITHER
typedef	uint32 (*LogLuv24fromXYZ_t)(float*, int );// = SGILOGENCODE_NODITHER
typedef	uint32 (*LogLuv32fromXYZ_t)(float*, int );// = SGILOGENCODE_NODITHER
#else
typedef	int (*LogL16fromY_t)(double, int);
typedef	int (*LogL10fromY_t)(double, int);
typedef	int (*uv_encode_t)(double, double, int);
typedef	uint32 (*LogLuv24fromXYZ_t)(float*, int);
typedef	uint32 (*LogLuv32fromXYZ_t)(float*, int);
#endif
#endif /* LOGLUV_PUBLIC */
    
typedef int (*TIFFCIELabToRGBInit_t)(TIFFCIELabToRGB*, TIFFDisplay *, float*);
typedef void (*TIFFCIELabToXYZ_t)(TIFFCIELabToRGB *, uint32, int32, int32,
			    float *, float *, float *);
typedef void (*TIFFXYZToRGB_t)(TIFFCIELabToRGB *, float, float, float,
			 uint32 *, uint32 *, uint32 *);

typedef int (*TIFFYCbCrToRGBInit_t)(TIFFYCbCrToRGB*, float*, float*);
typedef void (*TIFFYCbCrtoRGB_t)(TIFFYCbCrToRGB *, uint32, int32, int32,
			   uint32 *, uint32 *, uint32 *);

#if defined(c_plusplus) || defined(__cplusplus)
}
#endif

#endif /* _TIFFIO_ */

/* vim: set ts=8 sts=8 sw=8 noet: */
